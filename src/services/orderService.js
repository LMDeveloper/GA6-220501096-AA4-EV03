const BASE_URL = `${import.meta.env.VITE_API_BACKEND_URL}/order`;

export const filterOrdersByDate = async (from, to) => {
    const response = await fetch(`${BASE_URL}/filterByDate?from=${from}&to=${to}`, {
        method: 'GET',
        headers: {
            "Authorization": sessionStorage.getItem('token'),
            "Content-Type": "application/json"
        }
    });
    const reseponseBody = await response.json();
    const listedOrders = reseponseBody.object;
    return listedOrders;
}

export const getCustomersReport = (ordersList) => {

    // Crear un objeto para almacenar los contadores de pedidos por cliente.
    const customerOrderCounts = {};

    // Itera sobre cada orden en la lista de órdenes.
    for (const order of ordersList) {

        let customerId = null;

        // Obtiene el ID del cliente de la orden actual.
        if (order.customer.id === undefined) {
            customerId = order.customer;

        } else {
            customerId = order.customer.id;
        }


        // Si el ID del cliente no existe en el objeto 'customerOrderCounts', 
        // lo inicializa en 0.
        if (customerOrderCounts[customerId] === undefined) {
            customerOrderCounts[customerId] = 0;
        }

        // Incrementa el contador de pedidos para el cliente actual.
        customerOrderCounts[customerId]++;

        /*
        Al finalizar el bucle el objeto customerOrderCounts utiliza el 
         id del cliente como clave y el contador como valor (numero de ventas)    
          ej. {1:3, 2:2} (cliente 1 - 3 pedidos, cliente 2 - 2 pedidos)
        */
    }

    console.log(`customerOrderCounts ${JSON.stringify(customerOrderCounts)}`);

    // Crear una lista para almacenar los clientes que realizaron más de 2 pedidos.
    const topCustomers = [];

    // Itera sobre cada ID de cliente en el objeto 'customerOrderCounts'.
    for (const customerId in customerOrderCounts) {
        // Si el cliente realizó más de 2 pedidos, lo agrega a la lista 'topCustomers'.
        if (customerOrderCounts[customerId] >= 2) {
            topCustomers.push({
                customerId: customerId,
                orderCount: customerOrderCounts[customerId],
            });
        }
    }

    // Ordena los clientes por la mayor cantidad de pedidos en orden descendente.
    topCustomers.sort((a, b) => b.orderCount - a.orderCount);

    console.log(`topCustomers ${JSON.stringify(topCustomers)}`);

    // Devuelve la lista de clientes con más compras.
    return topCustomers;

}

export const getProductsReport = (ordersList) => {

    // Crear un objeto para almacenar los contadores de productos por orden.
    const productOrderCounts = {};

    // Itera sobre cada orden en la lista de órdenes.
    for (const order of ordersList) {

        let productId = null;
        let productName = null;

        for (const orderDetail of order.orderDetails) {

            productName = orderDetail.product.name;
            productId = orderDetail.product.id;
            console.log(`productId ${productId}`);
            console.log(`productName ${productName}`);

            if (productOrderCounts[productId] === undefined) {
                productOrderCounts[productId] = 0;
                console.log(`productOrderCounts[productId] ${productOrderCounts[productId]}`);
            }

            productOrderCounts[productId]++;
            console.log(`productOrderCounts ${JSON.stringify(productOrderCounts)}`);
        }


    }

    // Crear una lista para almacenar los productos que rotaron más de 2 veces.
    const topProducts = [];

    for (const productId in productOrderCounts) {
        if (productOrderCounts[productId] >= 2) {
            topProducts.push({
                productId: productId,
                productCount: productOrderCounts[productId]
            });
        }
    }

    topProducts.sort((a, b) => b.productCount - a.productCount);

    console.log(`topProducts ${JSON.stringify(topProducts)}`);

    return topProducts;

}

export const getTotalAmountOrders = (ordersList) => {

    let totalAmountSum = null;

    // Itera sobre cada orden en la lista de órdenes.
    for (const order of ordersList) {

        totalAmountSum += order.totalAmount;

        console.log(`totalAmountSum ${totalAmountSum}`);

    }

    return totalAmountSum.toFixed(2);

}

export const saveOrdersList = async (ordersList) => {

    try {
        const response = await fetch(`${BASE_URL}/saveOrders`, {
            method: 'POST',
            headers: {
                "Authorization": sessionStorage.getItem('token'),
                "Content-Type": "application/json"
            },
            body: JSON.stringify(ordersList)
        })

        const responseData = await response.json();
        if (response.status === 201) {
            // Procesar la respuesta exitosa
            console.log(responseData.message); // Acceder al mensaje
        } else if (response.status === 409) {
            // Procesar la respuesta con conflicto
            console.log(responseData.message); // Acceder al mensaje
        } else {
            // Procesar la respuesta con error
            console.error(responseData.message); // Acceder al mensaje
        }

        return responseData.message;

    } catch (error) {
        // Procesar el error
        console.error('Error saving orders:', error);
    }
}