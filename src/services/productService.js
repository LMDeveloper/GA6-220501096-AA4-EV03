const config = () => {
    return {
        headers: {
            "Authorization": sessionStorage.getItem('token'),
            "Content-Type": "application/json"
        }
    }
}

const BASE_URL = `${import.meta.env.VITE_API_BACKEND_URL}/products`;

export const selectProduct = async (id) => {
    const response = await fetch(`${BASE_URL}/${id}`, {
        method: 'GET',
        headers: {
            "Authorization": sessionStorage.getItem('token'),
            "Content-Type": "application/json"
        }
    });
    const reseponseBody = await response.json();
    const productSelected = reseponseBody.object;
    return productSelected;
}

export const searchProducts = async (query) => {
    const response = await fetch(`${BASE_URL}/filter?query=${query}`, {
        method: 'GET',
        headers: {
            "Authorization": sessionStorage.getItem('token'),
            "Content-Type": "application/json"
        }
    });
    const responseBody = await response.json();
    const productFound = responseBody.object;
    return productFound;
}

export const updateProduct = async (id, payload) => {
    try {
        const response = await fetch(`${BASE_URL}/${id}`, {
            method: 'PUT',
            headers: {
                "Authorization": sessionStorage.getItem('token'),
                "Content-Type": "application/json"
            },
            body: JSON.stringify(payload)
        });

        const responseData = await response.json();
        if (response.status === 201) {
            // Procesar la respuesta exitosa
            console.log(responseData.message); // Acceder al mensaje
        } else if (response.status === 409) {
            // Procesar la respuesta con conflicto
            console.log(responseData.message); // Acceder al mensaje
        } else {
            // Procesar la respuesta con error
            console.error(responseData.message); // Acceder al mensaje
        }

        return responseData.message;

    } catch (error) {
        console.error('Error updating product:', error);

    }
}

export const createProduct = async (payload) => {
    try {
        const response = await fetch(`${BASE_URL}`, {
            method: 'POST',
            headers: {
                "Authorization": sessionStorage.getItem('token'),
                "Content-Type": "application/json"
            },
            body: JSON.stringify(payload)
        });

        const responseData = await response.json();
        if (response.status === 201) {
            // Procesar la respuesta exitosa
            return responseData.message;
            console.log(responseData.message); // Acceder al mensaje
        } else if (response.status === 409) {
            return responseData.message;
            // Procesar la respuesta con conflicto
            console.log(responseData.message); // Acceder al mensaje
        } else {
            // Procesar la respuesta con error
            console.error(responseData.message); // Acceder al mensaje
            return responseData;
        }


    } catch (error) {
        console.error('Error updating product:', error);

    }
};

export const downloadExcelFile = async () => {
    try {
        const response = await fetch(`${BASE_URL}/downloadFile`, {
            method: 'GET',
            headers: {
                "Authorization": sessionStorage.getItem('token'),
                "Content-Type": "application/json"
            },
        });

        if (!response.ok) {
            throw new Error('Network response was not ok');
        }

        const blob = await response.blob();
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.href = url;
        a.download = 'products.xlsx';
        document.body.appendChild(a);
        a.click();
        a.remove();
        window.URL.revokeObjectURL(url);
    } catch (error) {
        console.error('There has been a problem with your fetch operation:', error);
    }
};


