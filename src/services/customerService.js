const config = () => {
    return {
        headers: {
            "Authorization": sessionStorage.getItem('token'),
            "Content-Type": "application/json"
        }
    }
}

const BASE_URL = `${import.meta.env.VITE_API_BACKEND_URL}/customer`;


export const getCustomers = async (query) => {
    const response = await fetch(`${BASE_URL}/filter?query=${query}`, {
        method: 'GET',
        headers: {
            "Authorization": sessionStorage.getItem('token'),
            "Content-Type": "application/json"
        }
    });
    //console.log(`response = ${response}`);
    const responseBody = await response.json();
    const clientsFound = responseBody.object;

    //console.log(`Clientes filtrados = ${clientsFound[0]}`);
    return clientsFound;
};

export const selectCustomer = async (id) => {
    const response = await fetch(`${BASE_URL}/${id}`, {
        method: 'GET',
        headers: {
            "Authorization": sessionStorage.getItem('token'),
            "Content-Type": "application/json"
        }
    });
    const reseponseBody = await response.json();
    const clientSelected = reseponseBody.object;
    return clientSelected;
};

export const createCustomer = async (payload) => {

    try {

        const response = await fetch(`${BASE_URL}`, {
            method: 'POST',
            headers: {
                "Authorization": sessionStorage.getItem('token'),
                "Content-Type": "application/json"
            },
            body: JSON.stringify(payload)
        });

        const responseData = await response.json();
        if (response.status === 201) {
            // Procesar la respuesta exitosa
            console.log(responseData.message); // Acceder al mensaje
        } else if (response.status === 409) {
            // Procesar la respuesta con conflicto
            console.log(responseData.message); // Acceder al mensaje
        } else {
            // Procesar la respuesta con error
            console.error(responseData.message); // Acceder al mensaje
        }

        return responseData.message;

    } catch (error) {
        // Procesar el error
        console.error('Error creating customer:', error);
    }
};

export const updateCustomer = async (id, payload) => {
    try {

        const response = await fetch(`${BASE_URL}/${id}`, {
            method: 'PUT',
            headers: {
                "Authorization": sessionStorage.getItem('token'),
                "Content-Type": "application/json"
            },
            body: JSON.stringify(payload)
        });

        const responseData = await response.json();
        if (response.status === 201) {
            // Procesar la respuesta exitosa
            console.log(responseData.message); // Acceder al mensaje
        } else if (response.status === 409) {
            // Procesar la respuesta con conflicto
            console.log(responseData.message); // Acceder al mensaje
        } else {
            // Procesar la respuesta con error
            console.error(responseData.message); // Acceder al mensaje
        }

        return responseData.message;

    } catch (error) {
        console.error('Error creating customer:', error);
    }
};

export const deleteCustomer = async (id) => {
    try {
        const response = await fetch(`${BASE_URL}/${id}`, {
            method: 'DELETE',
            headers: {
                "Authorization": sessionStorage.getItem('token'),
                "Content-Type": "application/json"
            },
        });

        const responseData = await response.json();
        if (response.status === 201) {
            // Procesar la respuesta exitosa
            console.log(responseData.message); // Acceder al mensaje
        } else if (response.status === 409) {
            // Procesar la respuesta con conflicto
            console.log(responseData.message); // Acceder al mensaje
        } else {
            // Procesar la respuesta con error
            console.error(responseData.message); // Acceder al mensaje
        }

        return responseData.message;

    } catch (error) {
        console.error('Error creating customer:', error);

    }
}