import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import { MainRoute } from './routes/MainRoute'
import { App } from './App'
// Import our custom CSS
//import './scss'
import './index.css'



// Import all of Bootstrap's JS

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
      {/* <MainRoute /> */}
    </BrowserRouter>
  </React.StrictMode>,
) 
