import { NavLink } from "react-router-dom"
import { Welcome } from "./Welcome"

export const SideBar = ( {handlerLogout} ) => {

    const navButtonStyle = 'md:grid md:gap-0 md:grid-cols-1 md:h-auto md:w-auto md:mx-2 md:p-[5px] sm:w-auto sm:m-4 sm:p-0 bg-transparent-bg rounded-xl sm:grid sm:justify-items-center outline-2 focus:outline outline-black';

    return (
        <>
            {/* <div class="grid grid-cols-3 h-screen place-content-center"> */}
                {/* <div class="w-screen"></div> */}
                <div class="bg-blue md:grid grid-cols-1 md:w-[230px] md:p-6 md:justify-self-end 
                    md:place-items-center sm:grid sm:grid-cols-1 sm:p-0 sm:gap-0 sm:w-48">
                    <NavLink className="nav-link-button" to="/consult-info">
                        <button 
                            class={navButtonStyle}>
                            <img class="md:size-28 sm:size-16" src="/consultar-informacion.png" alt="consultar información" />
                            <h2 class="md:text-sm sm:text-xs text-center">CONSULTAR INFORMACIÓN</h2>
                        </button>
                    </NavLink>
                    <NavLink className="nav-link-button" to="/update">
                        <button  class={navButtonStyle}>
                            <img class="md:size-28 md:mb-2 md:mr-4 sm:size-16 sm:m-2" src="/actualizar-informacion.png" alt="consultar información" />
                            <h2 class="md:text-sm sm:text-xs text-center">ACTUALIZAR INFORMACIÓN</h2>
                        </button>
                    </NavLink>
                    <button  class={navButtonStyle} onClick={handlerLogout}>
                        <img class="md:size-28 md:mb-2 md:mx-8 md:my-4 sm:size-16 sm:ml-2" src="/cerrar-sesion.png" alt="consultar información" />
                        <h2 class="md:text-sm sm:text-xs text-center">CERRAR SESIÓN</h2>
                    </button>
                </div>
                {/* <div> */}
                {/* <div class="bg-neutral-500 grid grid-cols-1 w-[700px] h-full p-6 place-items-center">
                    <h1 class="text-2xl text-neutral-800">Bienvenido!</h1>
                </div> */}
                    {/* <Welcome /> */}
                {/* </div> */}
            {/* </div> */}
        </>
    )
}