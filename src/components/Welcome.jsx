import { FaHome } from "react-icons/fa";

export const Welcome = ({backgroundMainStyle}) => {

    //const backgroundMainStyle = 'bg-neutral-400 grid grid-cols-1 w-[700px] h-full p-6 place-items-center';

    return (
        <>
            <div class={backgroundMainStyle}>
                {/* <h1 class="text-2xl text-neutral-800">Bienvenido!</h1> */}
                <FaHome class="w-[80px] h-[80px] color-black" />  
            </div>
        </>
    )
}