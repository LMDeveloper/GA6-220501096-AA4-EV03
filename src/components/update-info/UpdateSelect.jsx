import { NavLink } from "react-router-dom";

export const UpdateSelect = ({backgroundMainStyle, buttonStyle}) => {

    //const buttonStyle = "outline-4 hover:outline outline-gray-700/30 rounded-3xl border-4 border-gray-500/50 bg-gray-300 p-2 text-2xl ring-1 ring-gray-700 w-[400px]";

    return (
        <>
            <div class={backgroundMainStyle}>
                {/* <h1 class="text-2xl text-neutral-800">Consult-Info</h1> */}
                <div className="flex flex-wrap w-[600px] h-[400px] place-content-center">

                    <div class=" py-2">
                        <NavLink className="nav-link-button" to="/update/update-customer">
                            <button class={buttonStyle +" p-2 text-2xl w-[400px]"}>ACTUALIZAR CLIENTE</button>
                        </NavLink>
                    </div>
                    <div class=" py-2">
                        <NavLink className="nav-link-button" to="/update/update-product">
                            <button class={buttonStyle+" p-2 text-2xl w-[400px]"}>ACTUALIZAR PRODUCTOS</button>
                        </NavLink>
                    </div>
                    <div class="py-2">
                        <NavLink className="nav-link-button" to="/update/import-data">
                            <button class={buttonStyle+" p-2 text-2xl w-[400px]"}>IMPORTAR INFORMACIÓN</button>
                        </NavLink>
                    </div>
                </div>
            </div>
        </>
    )
}