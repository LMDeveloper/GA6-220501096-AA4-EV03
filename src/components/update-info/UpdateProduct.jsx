import { useState } from "react";
import { IoIosSearch } from "react-icons/io";
import { createProduct, searchProducts, updateProduct } from "../../services/productService";

export const UpdateProduct = ({ backgroundMainStyle, buttonStyle }) => {

    const inputNameStyle = "bg-zinc-300 w-32 italic font-extralight pl-2 pt-1 h-8 shadow-md shadow-neutral-600 border-r-4 border-neutral-900 border-double";
    const inputBoxStyle = "rounded-tr-2xl  bg-neutral-400 outline-2 focus:outline outline-neutral-400 ring-0 ring-neutral-400	text-sm h-8 w-52 text-inherit placeholder-zinc-700 shadow-md shadow-neutral-600 focus:bg-neutral-300";

    const [productSearch, setProductSearch] = useState('');
    const [productFound, setProductFound] = useState([]);

    const handleSearchProduct = async () => {
        if (!productSearch) return alert('Ingrese un nombre válido');
        const productResult = await searchProducts(productSearch);
        setProductFound(productResult);

    }

    const initialProductForm = {
        barcode: '',
        name: '',
        description: '',
        price: '',
        supplier: ''
    }

    //Hook para inicializar/llenar el payload product
    const [productForm, setProductForm] = useState(initialProductForm);
    const { id, barcode, name, description, price, supplier } = productForm;

    const handleEditProduct = (product) => {

        setProductForm(initialProductForm);

        const { id, barcode, name, description, price, supplier } = product;

        setProductForm(
            {
                id,
                barcode,
                name,
                description,
                price,
                supplier
            }
        );
    }

    const onInputChange = ({ target }) => {

        /*
         * Dentro de la función, se desestructura el evento
         * para obtener la propiedad target, que hace referencia
         * al elemento que desencadenó el evento, es decir, el
         * campo de entrada (input) en el que el usuario está
         * escribiendo.
         *
         * const { target } = event;
         */
        const { name, value } = target;

        setProductForm(
            {
                /**
                 * [name] contiene el nombre del campo de entrada que
                 * cambió (puede ser 'name', 'address' o 'phoneNumber').
                 * "value" contiene el valor que el usuario ha ingresado
                 * en el campo de entrada.
                 */
                ...productForm,
                [name]: value,
            }
        );
    };

    const handleUpdateProduct = async (productForm) => {

        if (productForm.name == '') return;
        if (productForm.description == '') return;
        if (productForm.price == '') return;

        const productUpdated = await updateProduct(productForm.id, productForm);
        if(productUpdated == undefined){
            alert('Acción no disponible');
            return;
        } else {
            alert(productUpdated);
        }
        window.location.reload();
    };

    const handleSaveProduct = async (productForm) => {
        if (productForm.name == '') return;
        if (productForm.description == '') return;
        if (productForm.price == '') return;

        const productInput = await createProduct(productForm);

        if(productInput == undefined){
            alert('Acción no disponible');
            return;
        } else if (productInput.startsWith('El producto con el código')) {
            alert(productInput);
            return;
        }
        else {
            alert(productInput);
        }

        window.location.reload();
    }

    //console.log(productForm);

    return (

        <>
            <div class={backgroundMainStyle}>
                <div class="flex flex-wrap h-full w-full ">
                    <div class="h-auto w-full ">
                        <div class="grid grid-row-1 grid-flow-col ">
                            <div class="grid justify-items-end">
                                <input
                                    type="text"
                                    placeholder=" Buscar Producto"
                                    class="rounded-tr-2xl bg-zinc	
                                outline-[2px] focus:outline outline-black m-2 text-md h-[38px] text-inherit placeholder-black
                                shadow-md shadow-black"
                                    onChange={(e) => setProductSearch(e.target.value)}
                                />

                            </div>
                            <div class="">
                                <button class={buttonStyle + " p-1"}
                                    onClick={() => handleSearchProduct()}
                                >
                                    <IoIosSearch class="h-8 w-8 ml-[2px]" />
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="w-full">
                        <div class="grid grid-cols-1 w-[650px] self-start">
                            <p class="text-xs font-light italic ">resultados</p>
                            <div id="table-wrapper" class="w-full">
                                <div id="table-scroll" class="scrollbar-thin scrollbar-webkit">
                                    <table class="table w-full table-auto ...">
                                        <thead class="table-header-group">
                                            <tr class="table-row ">
                                                <th>
                                                    Actualizar
                                                </th>
                                                <th
                                                    class="table-cell text-left px-1 border-dotted 
                                            border-neutral-500 border-r-2"
                                                >
                                                    Código
                                                </th>
                                                <th
                                                    class="table-cell text-left px-1 border-dotted 
                                            border-neutral-500 border-r-2"
                                                >
                                                    Nombre
                                                </th>
                                                <th
                                                    class="table-cell text-left px-1 border-dotted 
                                            border-neutral-500 border-r-2"
                                                >
                                                    Presentación
                                                </th>
                                                <th
                                                    class="table-cell text-left px-1 border-dotted 
                                            border-neutral-500 border-r-2"
                                                >
                                                    Precio
                                                </th>
                                                <th
                                                    class="table-cell text-left px-1"
                                                >
                                                    Proveedor
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                productFound && (
                                                    productFound.map(product => (
                                                        <tr key={product.id}>
                                                            <td>
                                                                <div className="flex flexbox place-content-center">

                                                                    <button
                                                                        onClick={() => handleEditProduct(product)}
                                                                    >
                                                                        <img class="h-4 w-4 m-[3px]" src="/pencil-icon-edit.png" />
                                                                    </button>
                                                                </div>


                                                                {/* <button
                                                                    onClick={() => handleRemoveProduct(product.id)}
                                                                >Eliminar</button> */}
                                                            </td>
                                                            <td>{product.barcode}</td>
                                                            <td>{product.name}</td>
                                                            <td>{product.description}</td>
                                                            <td>{product.price}</td>
                                                            <td>{product.supplier}</td>
                                                        </tr>
                                                    ))
                                                )
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-3 w-full">
                        <div class="col-span-2 pl-12 place-content-center">
                            <div class="grid  cols-1 h-min place-content-center">

                               
                                <div class="row-start-2 row-span-5">

                                    <div class="grid gap-2">
                                        <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-md">
                                            <div
                                                class={inputNameStyle}>
                                                Código
                                            </div>
                                            <input
                                                type="text"
                                                placeholder=""
                                                class={inputBoxStyle}
                                                name='barcode'
                                                value={barcode}
                                                onChange={onInputChange}
                                            />
                                        </div>
                                        <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-md">
                                            <div
                                                class={inputNameStyle}>
                                                Nombre
                                            </div>
                                            <input
                                                type="text"
                                                placeholder=""
                                                class={inputBoxStyle}
                                                name='name'
                                                value={name}
                                                onChange={onInputChange}
                                            />
                                        </div>
                                        <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-md">
                                            <div
                                                class={inputNameStyle}>
                                                Presentación
                                            </div>
                                            <input
                                                type="text"
                                                placeholder=""
                                                class={inputBoxStyle}
                                                name='description'
                                                value={description}
                                                onChange={onInputChange}
                                            />
                                        </div>
                                        <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-md">
                                            <div
                                                class={inputNameStyle}>
                                                Precio
                                            </div>
                                            <input
                                                type="text"
                                                placeholder=""
                                                class={inputBoxStyle}
                                                name='price'
                                                value={price}
                                                onChange={onInputChange}
                                            />
                                        </div>
                                        <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-lg">
                                            <div
                                                class={inputNameStyle}>
                                                Proveedor   
                                            </div>
                                            <input
                                                type="text"
                                                placeholder=""
                                                class={inputBoxStyle}
                                                name='supplier'
                                                value={supplier}
                                                onChange={onInputChange}
                                            />
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                        <div className="place-content-center">
                            <div class="grid grid-rows-3 cols-1 h-min place-content-center pr-8">

                                <div class="row-start-1 row-span-1">
                                    <button class={buttonStyle + " p-1"}
                                        onClick={() => handleSaveProduct(productForm)}
                                    >
                                        <img class="h-8 w-8 ml-[2px]" src="/guardar.png" />
                                    </button>
                                </div>

                                <div class="row-start-2 row-span-1">
                                    <button class={buttonStyle + " p-1"}
                                        onClick={() => handleUpdateProduct(productForm)}
                                    >
                                        <img class="h-8 w-8 ml-[2px]" src="/editar.png" />
                                    </button>
                                </div>

                                <div class="row-start-3 row-span-1">
                                    <button class={buttonStyle + " p-1"}
                                        onClick={() => setProductForm(initialProductForm)}
                                    >
                                        <img class="h-8 w-8 ml-[2px]" src="/eraser-icon.png" />
                                    </button>
                                </div>


                            </div>
                        </div>



                    </div>

                </div>

            </div>
        </>
    )
}