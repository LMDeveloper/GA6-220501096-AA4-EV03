import { useState } from "react";
import { IoIosSearch } from "react-icons/io"
import { createCustomer, deleteCustomer, getCustomers, selectCustomer, updateCustomer } from "../../services/customerService";

export const UpdateCustomer = ({ backgroundMainStyle, buttonStyle }) => {

    const initialCustomerForm = {
        name: '',
        phoneNumber: '',
        address: '',
        neighborhood: '',
        city: '',
        email: '',
    };

    const inputNameStyle = "bg-zinc-300 w-28 italic font-extralight pl-2 pt-1 h-8 shadow-md shadow-neutral-600 border-r-4 border-neutral-900 border-double";
    const inputBoxStyle = "rounded-tr-2xl  bg-neutral-400 outline-2 focus:outline outline-neutral-400 ring-0 ring-neutral-400	text-sm h-8 w-52 text-inherit placeholder-zinc-700 shadow-md shadow-neutral-600 focus:bg-neutral-300";

    const [customerSearch, setCustomerSearch] = useState('');

    const [customerFound, setCustomerFound] = useState([]);

    const handleSearchClient = async () => {
        //event.preventDefault();
        
        if (!customerSearch) return alert('Ingrese un nombre válido');
        
        const customerResult = await getCustomers(customerSearch);
        //console.log('Valor enviado:', customerSearch);
        //console.log(`Clientes encontrados ${customersFound[0].name}`);
        
        setCustomerFound(customerResult);
        // Aquí puedes llamar a una función que maneje el envío del valor
    };

    //Hook para inicializar/llenar el payload cliente
    const [customerForm, setCustomerForm] = useState(initialCustomerForm);
    const { id, name, phoneNumber, address, neighborhood, email, city } = customerForm;
    
    const handleSaveUpdateClient = async (customerForm) => {
        if (customerForm.name == '') return;
        if (customerForm.phoneNumber == '') return;
        if (customerForm.address == '') return;
        if (customerForm.neighborhood == '') return;
        if (customerForm.city == '') return;

        const customerInput = await createCustomer(customerForm);
        if(customerInput == undefined) {
            alert('Acción no disponible'); 
            return;
        } else if(customerInput == 'El cliente ya se encuentra registrado') {
            alert(customerInput);
            return;
        } else {
            alert(customerInput);
        }

        window.location.reload();
    }

    const handleEditCustomer = async (customer) => {
        
        setCustomerForm(initialCustomerForm);

        const { id, name, phoneNumber, address, neighborhood, email, city } = customer;

        setCustomerForm(
            {
                id,
                name,
                phoneNumber,
                address, 
                neighborhood, 
                email, 
                city
            }
        )
    }

    const handleUpdateCustomer = async (customerForm) => {
        if (customerForm.name == '') return;
        if (customerForm.phoneNumber == '') return;
        if (customerForm.address == '') return;
        if (customerForm.neighborhood == '') return;
        if (customerForm.city == '') return;

        const customerUpdated = await updateCustomer(customerForm.id, customerForm);
        if(customerUpdated == undefined) {
            alert('Acción no disponible'); 
            return;
        } else {
            alert(customerUpdated);
        }
        window.location.reload();

    }

    const handleRemoveCustomer = async (id) => {

        if (confirm("¿Estás seguro de que deseas eliminar el cliente id:" + id)) {

            const customerDeleted = await deleteCustomer(id);

            if (customerDeleted == undefined) {
                alert('Acción no disponible');
                return
            } else {
                alert(customerDeleted);
            }

            window.location.reload();
        }
    }


    const onInputChange = ({ target }) => {

        /*
         * Dentro de la función, se desestructura el evento
         * para obtener la propiedad target, que hace referencia
         * al elemento que desencadenó el evento, es decir, el
         * campo de entrada (input) en el que el usuario está
         * escribiendo.
         *
         * const { target } = event;
         */
        const { name, value } = target;

        setCustomerForm(
            {
                /**
                 * [name] contiene el nombre del campo de entrada que
                 * cambió (puede ser 'name', 'address' o 'phoneNumber').
                 * "value" contiene el valor que el usuario ha ingresado
                 * en el campo de entrada.
                 */
                ...customerForm,
                [name]: value,
            }
        );
    }

    //console.log(customerForm);

    return (
        <>
            <div class={backgroundMainStyle}>
                <div class="flex flex-wrap h-full w-full ">
                    <div class="h-auto w-full ">
                        <div class="grid grid-row-1 grid-flow-col ">
                            <div class="grid justify-items-end">
                                <input
                                    type="text"
                                    placeholder=" Buscar Cliente"
                                    class="rounded-tr-2xl bg-zinc	
                                outline-[2px] focus:outline outline-black m-2 text-md h-[38px] text-inherit placeholder-black
                                shadow-md shadow-black"
                                    value={customerSearch}
                                    onChange={(e) => setCustomerSearch(e.target.value)}
                                />

                            </div>
                            <div class="">
                                <button class={buttonStyle + " p-1"}
                                    onClick={() => handleSearchClient()}
                                >
                                    <IoIosSearch class="h-8 w-8 ml-[2px]" />
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="w-full">

                        <div class="grid grid-cols-1 w-[650px] self-start">
                            <p class="text-xs font-light italic ">resultados</p>
                            <div id="table-wrapper" class="w-full">
                                <div id="table-scroll" class="scrollbar-thin scrollbar-webkit">
                                    <table class="table w-full table-auto ...">
                                        <thead class="table-header-group">
                                            <tr class="table-row ">
                                                <th>
                                                    Actualizar
                                                </th>
                                                <th
                                                    class="table-cell text-left px-1 border-dotted 
                                            border-neutral-500 border-r-2 "
                                                >
                                                    Nombre
                                                </th>
                                                <th
                                                    class="table-cell text-left px-1 border-dotted 
                                            border-neutral-500 border-r-2"
                                                >
                                                    Dirección
                                                </th>
                                                <th
                                                    class="table-cell text-left px-1 border-dotted 
                                            border-neutral-500 border-r-2"
                                                >
                                                    Barrio
                                                </th>
                                                <th
                                                    class="table-cell text-left px-1 border-dotted 
                                            border-neutral-500 border-r-2"
                                                >
                                                    Ciudad
                                                </th>
                                                <th
                                                    class="table-cell text-left px-1 border-dotted 
                                            border-neutral-500 border-r-2"
                                                >
                                                    Teléfono
                                                </th>
                                                <th
                                                    class="table-cell text-left px-1"
                                                >
                                                    Email
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {customerFound && (
                                                customerFound.map(customer => (

                                                    <tr key={customer.id}>
                                                        <td>
                                                            <div className="flex flexbox place-content-center">
                                                                <button
                                                                    onClick={() => handleEditCustomer(customer)}
                                                                >
                                                                    <img class="h-4 w-4 m-[3px]" src="/pencil-icon-edit.png" />
                                                                    
                                                                </button>
                                                                <button
                                                                    onClick={() => handleRemoveCustomer(customer.id)}
                                                                >
                                                                    <img class="h-4 w-4 m-[3px]" src="/delete-icon-2.png" />
                                                                </button>
                                                            </div>
                                                        </td>
                                                        <td>{customer.name}</td>
                                                        <td>{customer.address}</td>
                                                        <td>{customer.neighborhood}</td>
                                                        <td>{customer.city}</td>
                                                        <td>{customer.phoneNumber}</td>
                                                        <td>{customer.email}</td>
                                                    </tr>
                                                ))
                                            )
                                            }


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="grid grid-cols-3 w-full">
                        <div class="col-span-2 pl-12 place-content-center">
                            <div class="grid  cols-1 h-min place-content-center">

                                {/* <div class="text-xs font-light italic self-end ">

                                    <img class="w-8 pb-2 ml-28" src="/editar.png" />
                                </div> */}
                                <div class="row-start-2 row-span-5">

                                    <div class="grid gap-2  ">
                                        <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-md">
                                            <div
                                                class={inputNameStyle}>
                                                Nombre
                                            </div>
                                            <input
                                                type="text"
                                                placeholder=""
                                                class={inputBoxStyle}
                                                name='name'
                                                value={name}
                                                onChange={onInputChange}
                                            />
                                        </div>
                                        <div className="flex flex-row h-8 border-b-1 border-neutral-400  shadow-md">
                                            <div
                                                class={inputNameStyle}>
                                                Dirección
                                            </div>
                                            <input
                                                type="text"
                                                placeholder=""
                                                class={inputBoxStyle}
                                                name='address'
                                                value={address}
                                                onChange={onInputChange}
                                            />
                                        </div>
                                        <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-md ">
                                            <div
                                                class={inputNameStyle}>
                                                Barrio
                                            </div>
                                            <input
                                                type="text"
                                                placeholder=""
                                                class={inputBoxStyle}
                                                name='neighborhood'
                                                value={neighborhood}
                                                onChange={onInputChange}
                                            />
                                        </div>
                                        <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-md">
                                            <div
                                                class={inputNameStyle}>
                                                Ciudad
                                            </div>
                                            <input
                                                type="text"
                                                placeholder=""
                                                class={inputBoxStyle}
                                                name='city'
                                                value={city}
                                                onChange={onInputChange}
                                            />
                                        </div>
                                        <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-md">
                                            <div
                                                class={inputNameStyle}>
                                                Teléfono
                                            </div>
                                            <input
                                                type="text"
                                                placeholder=""
                                                class={inputBoxStyle}
                                                name='phoneNumber'
                                                value={phoneNumber}
                                                onChange={onInputChange}
                                            />
                                        </div>
                                        <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-lg">
                                            <div
                                                class={inputNameStyle}>
                                                Email
                                            </div>
                                            <input
                                                type="text"
                                                placeholder=""
                                                class={inputBoxStyle}
                                                name='email'
                                                value={email}
                                                onChange={onInputChange}
                                            />
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                        <div className="place-content-center">
                            <div class="grid grid-rows-3 cols-1 h-min place-content-center pr-8">

                                <div class="row-start-1 row-span-1">
                                    <button class={buttonStyle + " p-1"}
                                        onClick={() => handleSaveUpdateClient(customerForm)}
                                    >
                                        <img class="h-8 w-8 ml-[2px]" src="/guardar.png" />
                                    </button>
                                </div>

                                <div class="row-start-2 row-span-1">
                                    <button class={buttonStyle + " p-1"}
                                        onClick={() => handleUpdateCustomer(customerForm)}
                                    >
                                        <img class="h-8 w-8 ml-[2px]" src="/editar.png" />
                                    </button>
                                </div>
                                
                                <div class="row-start-3 row-span-1">
                                    <button class={buttonStyle + " p-1"}
                                        onClick={() => setCustomerForm(initialCustomerForm)}
                                    >
                                        <img class="h-8 w-8 ml-[2px]" src="/eraser-icon.png" />
                                    </button>
                                </div>


                            </div>
                        </div>


                    </div>

                </div>

            </div>
        </>
    )
}