import React, { useState } from "react";
import * as XLSX from 'xlsx';
import { LiaFileUploadSolid } from "react-icons/lia";
import { saveOrdersList } from "../../services/orderService";
import { downloadExcelFile } from "../../services/productService";


const ImportData = ({ backgroundMainStyle, buttonStyle }) => {

    /**
     * Estado para almacenar los pedidos, el nombre del archivo 
     * y el número máximo de detalles del pedido
     */
    const [orders, setOrders] = useState([]);
    const [fileName, setFileName] = useState('');
    const [maxOrderDetails, setMaxOrderDetails] = useState(0);

    /**
     * Función para convertir una fecha de Excel a una fecha de JavaScript
     */
    // const excelDateToJSDate = (serial) => {
    //     const utcDays = Math.floor(serial - 25569);
    //     const utcValue = utcDays * 86400;
    //     const dateInfo = new Date(utcValue * 1000);

    //     const date = new Date(dateInfo.getFullYear(), dateInfo.getMonth(), dateInfo.getDate());
    //     return date.toISOString().split('T')[0]; // Formato YYYY-MM-DD
    // };


    /**
 * Función para convertir una fecha de Excel a una fecha de JavaScript
 */
    const excelDateToJSDate = (serial) => {
        // Calcula los días UTC desde el 1 de enero de 1970
        const utcDays = serial - 25569;
        // Convierte días UTC a milisegundos
        const utcValue = utcDays * 86400 * 1000;
        // Crea un objeto Date en UTC
        const dateInfo = new Date(utcValue);

        // Devuelve la fecha en formato ISO sin la parte de tiempo
        return dateInfo.toISOString().split('T')[0]; // Formato YYYY-MM-DD
    };

    // Función para manejar la carga del archivo
    const handleFileUpload = (event) => {

        // Obtiene el primer archivo seleccionado
        const file = event.target.files[0];
        const reader = new FileReader();

        const fileName = file.name;

        // Almacena el nombre del archivo en el estado
        setFileName(fileName);

        // document.getElementById('file-name').textContent = fileName;

        reader.onload = (e) => {
            const data = new Uint8Array(e.target.result);
            const workbook = XLSX.read(data, { type: 'array' });
            const sheetName = workbook.SheetNames[0];
            const worksheet = workbook.Sheets[sheetName];
            const jsonData = XLSX.utils.sheet_to_json(worksheet);

            let maxDetails = 0;

            // Formatea los datos del archivo Excel
            const formattedData = jsonData.map(row => {
                const orderDetails = [];
                let i = 1;

                // Recorre los detalles del pedido
                while (row[`orderDetail${i}_productId`]) {
                    orderDetails.push({
                        quantity: row[`orderDetail${i}_quantity`],
                        product: { id: row[`orderDetail${i}_productId`] }
                    });
                    i++;
                }

                // Actualiza el número máximo de detalles del pedido
                if (i - 1 > maxDetails) {
                    maxDetails = i - 1;
                }

                return {
                    order: {
                        paymentMode: row.paymentMode,
                        totalAmount: row.totalAmount,
                        orderDate: typeof row.orderDate === 'number' ? excelDateToJSDate(row.orderDate) : row.orderDate
                    },
                    customer: {
                        name: row.customerName,
                        phoneNumber: row.customerPhoneNumber,
                        address: row.customerAddress,
                        neighborhood: row.customerNeighborhood,
                        city: row.customerCity
                    },
                    orderDetail: orderDetails
                };
            });

            // Almacena el número máximo de detalles del pedido en el estado
            setMaxOrderDetails(maxDetails);

            // Almacena los datos formateados en el estado
            setOrders(formattedData);
        };

        reader.readAsArrayBuffer(file);
    };

    console.log(`orders imported ${JSON.stringify(orders)}`);
    console.log(`File name ${fileName}`);

    // Función para manejar la acción de enviar (guardar) los pedidos
    const handleSubmit = async (orders) => {
        try {

            if (orders.length === 0) {
                alert('Importe un archivo segun la plantilla descargada');
                return; // Detener la ejecución si la lista de pedidos está vacía



            } else {

                const result = confirm('Desea guardar las órdenes importadas');
                if (result) {
                    const response = await saveOrdersList(orders);

                    if (response == undefined) {
                        alert('Acción no disponible');
                        return;
                    } else {

                        alert(response);
                        console.log('Orders saved successfully:', orders);
                    }
                }

                window.location.reload();
            }

        } catch (error) {
            console.error('Error saving orders:', error);
        }
    };

    const generateAndDownloadTemplate = () => {
        const headers = [
            "paymentMode",
            "totalAmount",
            "orderDate",
            "customerName",
            "customerPhoneNumber",
            "customerAddress",
            "customerNeighborhood",
            "customerCity",
            "orderDetail1_productId",
            "orderDetail1_quantity"
        ];

        const worksheet = XLSX.utils.aoa_to_sheet([headers]);

        // Define cell styles
        const headerStyle = {
            font: { bold: true, color: { rgb: "FFFFFF" } },
            fill: { fgColor: { rgb: "000000" } }
        };

        // Apply styles to header cells
        headers.forEach((header, index) => {
            const cellAddress = XLSX.utils.encode_cell({ r: 0, c: index });
            if (!worksheet[cellAddress]) {
                worksheet[cellAddress] = { t: 's', v: header };
            }
            worksheet[cellAddress].s = headerStyle;
        });

        const workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, "Template");

        XLSX.writeFile(workbook, "template.xlsx");
    };

    const getProductsList = async () => {
        await downloadExcelFile();
    }

    return (
        <>
            <div class={backgroundMainStyle}>
                <div className="flex flex-wrap h-full w-full place-items-center">
                    <div className="grid grid-cols-2 w-full h-[110px]">

                        <div className="place-self-center">
                            <div className="flex flex-nowrap items-center hover:scale-110 ">
                                <div className={buttonStyle + " w-8 h-8"}>
                                    <input
                                        className="hidden"
                                        id="files"
                                        type="file"
                                        accept=".xlsx, .xls"
                                        name='name'
                                        onChange={handleFileUpload} />

                                    <label for="files">
                                        <LiaFileUploadSolid className="h-8 w-8" />

                                    </label>
                                </div>
                                <p className="pl-0 text-sm">Subir Archivo </p>
                            </div>
                        </div>

                        <div className="place-self-center">
                            <div className="w-[210px]">
                                <button className={buttonStyle + " w-[210px]"} onClick={generateAndDownloadTemplate}>
                                    Descargar plantilla
                                </button>
                                <button className={buttonStyle + " w-[210px]"} onClick={getProductsList}>
                                    Descargar ID Productos
                                </button>
                            </div>
                        </div>

                    </div>

                    <div id="table-wrapper" class="w-full h-max">
                        <p>{fileName}</p>
                        <div id="table-scroll-2" class="scrollbar-thin scrollbar-webkit">
                            <table class="table w-full table-auto ...">
                                <thead class="table-header-group">

                                    <tr class="table-row ">
                                        <th>
                                            Método de Pago
                                        </th>
                                        <th
                                            class="table-cell text-left px-1 border-dotted 
                                                        border-neutral-500 border-r-2"
                                        >
                                            Total
                                        </th>
                                        <th
                                            class="table-cell text-left px-1 border-dotted 
                                                        border-neutral-500 border-r-2"
                                        >
                                            Fecha
                                        </th>
                                        <th
                                            class="table-cell text-left px-1 border-dotted 
                                                        border-neutral-500 border-r-2"
                                        >
                                            Nombre
                                        </th>
                                        <th
                                            class="table-cell text-left px-1 border-dotted 
                                                        border-neutral-500 border-r-2"
                                        >
                                            Teléfono
                                        </th>
                                        <th
                                            class="table-cell text-left px-1"
                                        >
                                            Dirección
                                        </th>
                                        <th
                                            class="table-cell text-left px-1"
                                        >
                                            Barrio
                                        </th>
                                        <th
                                            class="table-cell text-left px-1"
                                        >
                                            Ciudad
                                        </th>
                                        {/* Genera dinámicamente las columnas de detalles del pedido */}
                                        {[...Array(maxOrderDetails)].map((_, index) => (
                                            <React.Fragment key={index}>
                                                <th className="table-cell text-left px-1 border-dotted border-neutral-500 border-r-2">{`Cantidad ${index + 1}`}</th>
                                                <th className="table-cell text-left px-1 border-dotted border-neutral-500 border-r-2">{`Producto ${index + 1}`}</th>
                                            </React.Fragment>
                                        ))}

                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        orders && (
                                            orders.map(order => (
                                                <tr>
                                                    <td>
                                                        {order.order.paymentMode}
                                                    </td>
                                                    <td>
                                                        {order.order.totalAmount}
                                                    </td>
                                                    <td>
                                                        {order.order.orderDate}
                                                    </td>
                                                    <td>
                                                        {order.customer.name}
                                                    </td>
                                                    <td>
                                                        {order.customer.phoneNumber}
                                                    </td>
                                                    <td>
                                                        {order.customer.address}
                                                    </td>
                                                    <td>
                                                        {order.customer.neighborhood}
                                                    </td>
                                                    <td>
                                                        {order.customer.city}
                                                    </td>
                                                    {/* Renderiza los detalles del pedido */}
                                                    {order.orderDetail.map((detail, detailIndex) => (
                                                        <React.Fragment key={detailIndex}>
                                                            <td>{detail.quantity}</td>
                                                            <td>{detail.product.id}</td>
                                                        </React.Fragment>
                                                    ))}
                                                    {/* Agrega celdas vacías si faltan detalles del pedido */}
                                                    {[...Array(maxOrderDetails - order.orderDetail.length)].map((_, emptyIndex) => (
                                                        <React.Fragment key={emptyIndex}>
                                                            <td></td>
                                                            <td></td>
                                                        </React.Fragment>
                                                    ))}

                                                </tr>
                                            ))
                                        )
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div className="grid grid-cols-1 w-full h-[110px]">
                        <div className="place-self-center">

                            <button class={buttonStyle + " p-1"}
                                onClick={() => handleSubmit(orders)}
                            >
                                <img class="h-8 w-8 ml-[2px]" src="/guardar.png" />
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};
export default ImportData;