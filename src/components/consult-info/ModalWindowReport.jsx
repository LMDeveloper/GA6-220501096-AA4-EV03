import { useEffect, useState } from "react";
import { selectCustomer } from "../../services/customerService";
import { selectProduct } from "../../services/productService";

const ModalWindowReport = ({ onClose, customerList, productsList, totalAmount }) => {

    const [customerNames, setCustomerNames] = useState([]);
    const [productNames, setProductNames] = useState([]);

    const handleCustomerNameById = async (id) => {
        const customerName = await selectCustomer(id);
        return customerName.name;
    }

    const handleProductNameById = async (id) => {
        const productName = await selectProduct(id);
        return productName.name;
    }

    const fetchCustomerNames = async () => {
        const names = {};
        for (const customer of customerList) {
            const name = await handleCustomerNameById(customer.customerId);
            names[customer.customerId] = name;
        }
        console.log(names);
        setCustomerNames(names);
    };

    const fetchProductNames = async () => {
        const names = {};
        for (const product of productsList) {
            const name = await handleProductNameById(product.productId);
            names[product.productId] = name;
        }
        console.log(names);
        setProductNames(names);
    };

    useEffect(() => {

        fetchCustomerNames();
        fetchProductNames();

    }, []);

    return (
        <div>
            <div className='fixed inset-0 right-30 bg-black
            bg-opacity-25 backdrop-blur-sm flex
            justify-center items-center z-30 '
            >
                <div className='w-[315x] px-3 sm:w-[auto] sm:px-0
                flex flex-col z-40'
                >

                    <button className='text-white text-xl
                        place-self-end'
                        onClick={() => onClose()}
                    >
                        x
                    </button>
                    <div className='bg-sky-blue p-2 rounded h-[auto]
                    flex flex-col space-y-4
                    '>
                        <div className="grid justify-items-stretch p-2">
                            <h1 className="text-xl text-center font-semibold">
                                Balance
                            </h1>
                            <h3 className="text-base font-medium">
                                Total Ventas: ${totalAmount}
                            </h3>
                            <h3 className="text-base font-medium">
                                Clientes Recurrentes:
                            </h3>
                            <div className="justify-self-center py-2 w-[500px]">
                                <div id="table-wrapper" class="w-auto">
                                    <div id="table-scroll-3" class="scrollbar-thin scrollbar-webkit">
                                        <table class="table-auto">
                                            <thead className="bg-zinc   ">
                                                <tr>
                                                    <th className="font-semibold">Nombre</th>
                                                    <th class="text-center font-semibold">Cantidad de Pedidos</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {customerList && (

                                                    customerList.map(customer => (

                                                        <tr key={customer.customerId}>
                                                            <td class="text-center">{customerNames[customer.customerId] || 'Cargando...'}</td>
                                                            <td class="text-center">{customer.orderCount}</td>
                                                        </tr>
                                                    ))
                                                )}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <h3 className="text-base font-medium">
                                Productos más vendidos:
                            </h3>
                            <div className="justify-self-center py-2 w-[500px]">
                                <div id="table-wrapper" class="w-auto">
                                    <div id="table-scroll-3" class="scrollbar-thin scrollbar-webkit">
                                        <table class="table-auto">
                                            <thead className="bg-zinc   ">
                                                <tr>
                                                    <th className="font-semibold">Nombre</th>
                                                    <th class="text-center font-semibold">Cantidad de Pedidos</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {productsList && (
                                                    productsList.map(product => (

                                                        <tr key={product.productId}>
                                                            <td class="text-center">{productNames[product.productId]}</td>
                                                            <td class="text-center">{product.productCount}</td>
                                                        </tr>
                                                    )) 
                                                )}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    )

}

export default ModalWindowReport;