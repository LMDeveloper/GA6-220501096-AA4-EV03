import { useState } from "react";
import { IoIosSearch } from "react-icons/io";
import { getCustomers, selectCustomer } from "../../services/customerService";
import { de, tr } from "date-fns/locale";
import { MdPersonSearch } from "react-icons/md";


export const ConsultClient = ({ backgroundMainStyle, buttonStyle }) => {

    const [customerSearch, setCustomerSearch] = useState('');

    const [customerFound, setCustomerFound] = useState([]);

    const [orders, setOrders] = useState([]);

    const handleSearchClient = async () => {
        //event.preventDefault();

        if (!customerSearch) return alert('Ingrese un nombre válido');

        const customerResult = await getCustomers(customerSearch);
        //console.log('Valor enviado:', customerSearch);
        //console.log(`Clientes encontrados ${customersFound[0].name}`);

        setCustomerFound(customerResult);
        // Aquí puedes llamar a una función que maneje el envío del valor
    };

    const handleFilterOrders = async (idClientSelected) => {

        const ordersResult = await selectCustomer(idClientSelected);

        setOrders(ordersResult.orders);

        console.log(`Orders: ${ordersResult.orders[0].orderDate}`);
    };

    return (
        <>
            <div class={backgroundMainStyle}>

                <div class="flex flex-row gap-4 ">
                    <div class="content-center">
                        <input
                            type="text"
                            placeholder=" Buscar Cliente"
                            class="rounded-tr-2xl bg-zinc	
                                outline-[2px] focus:outline outline-black m-2 text-md h-[38px] text-inherit placeholder-black
                                shadow-md shadow-black"
                            value={customerSearch}
                            onChange={(e) => setCustomerSearch(e.target.value)}
                        />

                    </div>
                    <div class="">
                        {/*Botón de Búsqueda*/}
                        <button class={buttonStyle + " p-1"}
                            onClick={() => handleSearchClient()}
                        >
                            <IoIosSearch class="h-8 w-8 ml-[2px]" />
                        </button>
                    </div>
                </div>
                
                <div class="grid grid-cols-1 w-[600px] self-start">
                    <p class="text-xs font-light italic ">resultados</p>
                    <div id="table-wrapper" class="w-full">
                        <div id="table-scroll" class="scrollbar-thin scrollbar-webkit">
                            <table class="table w-full table-auto ...">
                                <thead class="table-header-group">
                                    <tr class="table-row ">
                                        <th>
                                            Consultar
                                        </th>
                                        <th
                                            class="table-cell text-left px-1 border-dotted 
                                            border-neutral-500 border-r-2 "
                                        >
                                            Nombre
                                        </th>
                                        <th
                                            class="table-cell text-left px-1 border-dotted 
                                            border-neutral-500 border-r-2"
                                        >
                                            Dirección
                                        </th>
                                        <th
                                            class="table-cell text-left px-1 border-dotted 
                                            border-neutral-500 border-r-2"
                                        >
                                            Ciudad
                                        </th>
                                        <th
                                            class="table-cell text-left px-1 border-dotted 
                                            border-neutral-500 border-r-2"
                                        >
                                            Teléfono
                                        </th>
                                        <th
                                            class="table-cell text-left px-1"
                                        >
                                            Email
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {customerFound && (
                                        customerFound.map(customer => (

                                            <tr key={customer.id}>
                                                <td>
                                                    <button
                                                        onClick={() => handleFilterOrders(customer.id)}
                                                    >
                                                        Ver Ordenes
                                                    </button>
                                                </td>
                                                <td>{customer.name}</td>
                                                <td>{customer.address}</td>
                                                <td>{customer.city}</td>
                                                <td>{customer.phoneNumber}</td>
                                                <td>{customer.email}</td>
                                            </tr>
                                        ))
                                    )
                                    }


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="grid grid-cols-1 w-[600px] self-start ">
                    <p class="text-xs font-light italic ">Historial de Compras</p>
                    <div id="table-wrapper" >
                        <div id="table-scroll" class="scrollbar-thin scrollbar-webkit">
                            <table class="table w-full ...">
                                <thead class="table-header-group bg-zinc-300 ">
                                    <tr class="table-row ">
                                        <th
                                            class="table-cell text-left px-1 border-dotted 
                                        border-neutral-500 border-r-2"
                                        >
                                            Fecha
                                        </th>
                                        <th
                                            class="table-cell text-left px-1 border-dotted 
                                        border-neutral-500 border-r-2"
                                        >
                                            OrdenId
                                        </th>
                                        <th
                                            class="table-cell text-left px-1 border-dotted 
                                        border-neutral-500 border-r-2"
                                        >
                                            Detalle
                                        </th>
                                        <th
                                            class="table-cell text-left px-1 w-36"
                                        >
                                            Total
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {orders && (
                                        orders.map(order => (

                                            <tr key={order.id}>
                                                <td>{order.orderDate}</td>
                                                <td>{order.id}</td>
                                                <td>
                                                    {order.orderDetails.map(detail => (
                                                        ' Producto: ' +
                                                        detail.product.name + ', ' +
                                                        ' Cantidad: ' +
                                                        detail.quantity
                                                    ))}
                                                    {" "}
                                                </td>
                                                <td>{order.totalAmount}</td>
                                            </tr>
                                        )))
                                    }
                                </tbody>

                                {/* <div class="table-row-group">
                            <div class="table-row">
                                <div class="table-cell ..."></div>
                                <div class="table-cell ..."></div>
                                <div class="table-cell ..."></div>
                            </div>
                        </div> */}
                            </table>
                        </div>
                    </div>
                    {/* <h2>Buscar Cliente {nombre}</h2> */}
                </div>
            </div>

        </>
    )
}