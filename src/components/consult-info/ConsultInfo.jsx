import { NavLink } from "react-router-dom";

export const ConsultInfo = ({backgroundMainStyle, buttonStyle}) => {

    //const buttonStyle = "outline-4 hover:outline outline-gray-700/30 rounded-3xl border-4 border-gray-500/50 bg-gray-300 p-2 text-2xl ring-1 ring-gray-700 w-[400px]";

    return (
        <>
            <div class={backgroundMainStyle}>
                {/* <h1 class="text-2xl text-neutral-800">Consult-Info</h1> */}
                <div class="self-end pb-2">
                    <NavLink className="nav-link-button" to="/consult-info/client">
                        <button class={buttonStyle+" p-2 text-2xl w-[400px]"}>INFORMACIÓN DE CLIENTES</button>
                    </NavLink>
                </div>
                <div class="self-start pt-2">
                    <NavLink className="nav-link-button" to="/consult-info/sales">

                        <button class={buttonStyle+" p-2 text-2xl w-[400px]"}>REPORTE DE VENTAS</button>
                    </NavLink>
                </div>
            </div>
        </>
    )
}