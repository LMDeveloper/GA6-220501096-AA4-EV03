import React, { useEffect, useState } from 'react';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { createTheme } from '@mui/material/styles';
import { ThemeProvider } from '@mui/material/styles';
import { IoIosSearch } from 'react-icons/io';
import { TbReportSearch } from 'react-icons/tb';
import { filterOrdersByDate, getCustomersReport, getProductsReport, getTotalAmountOrders } from '../../services/orderService';
import dayjs from 'dayjs';
import ModalWindowReport from './ModalWindowReport';


export const ConsultSales = ({ backgroundMainStyle, buttonStyle }) => {

    const mensajeAlert = 'Ingrese un rango de fechas valido';

    const [selectedDateFrom, setSelectedDateFrom] = useState(null);

    const handleDateChangeFrom = (date) => {
        setSelectedDateFrom(date);
    };

    const [selectedDateTo, setSelectedDateTo] = useState(null);

    const handleDateChangeTo = (date) => {
        setSelectedDateTo(date);
    };

    const formatDate = (date) => {
        if (date) {
            return dayjs(date).format('YYYY-MM-DD');
        }
        return '';
    };

    const [ordersListed, setOrdersListed] = useState([]);

    const handleFilterOrders = async (from, to) => {
        if (!from || !to) { alert(mensajeAlert); return; };

        if (dayjs(to).isBefore(dayjs(from))) { alert(mensajeAlert); return; };

        const filteredOrders = await filterOrdersByDate(
            formatDate(from), formatDate(to));

        console.log(filteredOrders);

        setOrdersListed(filteredOrders)

        //return filteredOrders;

    };

    const [customerList, setCustomerList] = useState([]);
    const [productsList, setProductsList] = useState([]);
    const [totalAmount, setTotalAmount] = useState();


    const handleReportOrders = (orderList) => {

        if (!orderList.length) {

            alert(mensajeAlert); return;

        } else {

            const topCustomersList = getCustomersReport(orderList);

            const topProductList = getProductsReport(orderList);

            const getTotal = getTotalAmountOrders(orderList);

            setCustomerList(topCustomersList);

            setProductsList(topProductList);

            setTotalAmount(getTotal);

            setShowModal(true);

            console.log(`Ejecucion boton handleReportOrders ${topCustomersList}`);
            console.log(`Ejecucion boton handleReportOrders ${topProductList}`);
        }

    }

 


    const [showModal, setShowModal] = useState(false);

    useEffect(() => {
        //alert('ejemplo');
        // const orders = filterOrdersByDate('2014-05-12', '2024-06-12');
        // console.log(orders);
        //console.log(`${formatDate(selectedDateFrom)}, ${selectedDateTo}`);

        //handleFilterOrders()

        //console.log(filteredOrders);
    }, [handleFilterOrders]);

    const datePickerStyle = 'flex items-center justify-center shadow-lg shadow-neutral-600 ';

    const theme = createTheme({
        // Configuración de tu tema aquí
        // typography: {
        //     fontSize: 16,

        // },
        palette: {
            primary: {
                main: '#020617',
            },
            info: {
                main: '#020617',
            },

            mode: 'light',
            contrastThreshold: 2,
        },

        components: {
            MuiOutlinedInput: {
                styleOverrides: {
                    notchedOutline: {
                        color: '#020617',
                        borderBottomColor: '#020617', // Cambia el borde al hacer click
                        borderLeftColor: '#020617',
                        borderTopColor: '#525252',
                        borderRightColor: '#525252',
                        borderWidth: 1,
                        //borderTopRightRadius: '1.8rem',
                    },
                },
            },
            MuiFormLabel: {
                styleOverrides: {
                    root: {
                        color: '#020617',

                    }
                }
            },
            MuiInputBase: {
                styleOverrides: {
                    root: {

                    }
                }
            }
        },

    });

    return (
        <>

            <div class={backgroundMainStyle} >

                {showModal && (
                    <ModalWindowReport onClose={() => setShowModal(false)}
                        customerList={customerList}
                        productsList={productsList}
                        totalAmount={totalAmount}
                    />
                )}



                <div class="flex-wrap h-full w-full">

                    <div class="flex flex-nowrap items-center px-8 gap-8 h-[150px]">
                        <div class={datePickerStyle}>
                            <LocalizationProvider dateAdapter={AdapterDayjs} >
                                <DemoContainer components={['DatePicker']} >
                                    <ThemeProvider theme={theme}>

                                        <DatePicker
                                            label="Desde"
                                            value={selectedDateFrom}
                                            onChange={handleDateChangeFrom}
                                        />
                                    </ThemeProvider>
                                </DemoContainer>
                            </LocalizationProvider>

                        </div>

                        <div class={datePickerStyle}>
                            <LocalizationProvider dateAdapter={AdapterDayjs} >
                                <DemoContainer components={['DatePicker']} >
                                    <ThemeProvider theme={theme}>

                                        <DatePicker
                                            label="Hasta"
                                            value={selectedDateTo}
                                            onChange={handleDateChangeTo}

                                        />
                                    </ThemeProvider>
                                </DemoContainer>
                            </LocalizationProvider>

                        </div>

                        <div class="flex items-center justify-center pt-2">
                            <button
                                class={buttonStyle + " p-1"}
                                onClick={() => handleFilterOrders(selectedDateFrom, selectedDateTo)}
                            >
                                <IoIosSearch class="h-8 w-8 ml-[2px]" />
                            </button>
                        </div>
                    </div>




                    <div class="w-full h-auto self-start">
                        <h2 class='text-xs font-light italic' >Tabla de Ordenes</h2>

                        <div id="table-wrapper" class="w-auto">
                            <div id="table-scroll-2" class="scrollbar-thin scrollbar-webkit">
                                <table class="table  ...">
                                    <thead class="table-header-group bg-zinc-300 ">
                                        <tr class="table-row ">
                                            <th
                                                class="table-cell text-left px-1 border-dotted 
                                        border-neutral-500 border-r-2"
                                            >
                                                Fecha
                                            </th>
                                            <th
                                                class="table-cell text-left px-1 border-dotted 
                                        border-neutral-500 border-r-2"
                                            >
                                                Orden
                                            </th>
                                            <th
                                                class="table-cell text-left px-1 border-dotted 
                                        border-neutral-500 border-r-2"
                                            >
                                                Total
                                            </th>
                                            <th
                                                class="table-cell text-left px-1"
                                            >
                                                Método de pago
                                            </th>
                                        </tr>
                                    </thead>
                                    {/* <div class="table-row-group">
                                <div class="table-row">
                                    <div class="table-cell ...">1</div>
                                    <div class="table-cell ..."></div>
                                    <div class="table-cell ..."></div>
                                </div>
                            </div> */}
                                    <tbody>
                                        {ordersListed && (
                                            ordersListed.map(order => (
                                                <tr key={order.id}>
                                                    <td>{order.orderDate}</td>
                                                    <td>{order.id}</td>
                                                    <td>{order.totalAmount}</td>
                                                    <td>{order.paymentMode}</td>
                                                </tr>
                                            ))
                                        )}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                    <div class="flex flex-nowrap items-center place-content-center px-8 gap-8 h-auto pt-12">
                        <button class={buttonStyle + " p-1"}
                            onClick={() => handleReportOrders(ordersListed)}
                        >
                            <TbReportSearch class="h-8 w-8 ml-[2px]" />
                        </button>
                    </div>


                </div>

            </div>


        </>
    )

}