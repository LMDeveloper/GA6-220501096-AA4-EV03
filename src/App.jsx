import { Navigate, Route, Routes, useNavigate } from 'react-router-dom';
import { useAuth } from './auth/hooks/useAuth';
import { LoginPage } from './auth/pages/LoginPage';
import { MainRoute } from './routes/MainRoute';
import { useEffect } from 'react';
import { LoginAdminPage } from './auth/pages/LoginAdminPage';
// import { Navbar } from './components/layout/Navbar';
// import { UsersPage } from './pages/UsersPage';

export const App = () => {

    //Métodos para gestionar el inicio y cierre de sesión
    const { login, handlerLogin, handlerLogout } = useAuth();

    //console.log(`Variable de entorno ${import.meta.env.VITE_API_BASE_URL} Variable privada ${import.meta.env.DB_PASSWORD}`);

    return (
        <Routes>
            {/* El parametro isAuth del objeto login determina 
            si se muestra la pagina de inicio de sesión o la 
            página de usuarios */}
            {
                login.isAuth
                    ? (
                        <Route
                            path='*'
                            element={<MainRoute handlerLogout={handlerLogout} />}
                        />

                    )
                    : <>

                        <Route path='/login'
                            element={<LoginPage handlerLogin={handlerLogin} />}
                        />



                        <Route path='/*' element={<Navigate to="/login" />} />
                    </>
            }

            {/* <Route path='/loginAdmin'
                element={<LoginAdminPage
                    login={login}
                    handlerLogin={handlerLogin}
                    handlerLogout={handlerLogout} />}
            /> */}

            <Route path='/handleUsers'
                element={<LoginAdminPage
                    // login={login}
                    // handlerLogin={handlerLogin}
                    // handlerLogout={handlerLogout} 
                    />}
            />

        </Routes>
    );
}