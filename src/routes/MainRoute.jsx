import { SideBar } from "../components/SideBar"
import { Navigate, Route, Routes } from "react-router-dom"
import { Welcome } from "../components/Welcome"
import { ConsultInfo } from "../components/consult-info/ConsultInfo"
import { ConsultClient } from "../components/consult-info/ConsultClient"
import { ConsultSales } from "../components/consult-info/ConsultSales"
import { UpdateSelect } from "../components/update-info/UpdateSelect"
import { UpdateCustomer } from "../components/update-info/UpdateCustomer"
import { UpdateProduct } from "../components/update-info/UpdateProduct"
import ImportData from "../components/update-info/ImportData"


/*
 * El componente MainRoute define las rutas de navegación
 * de la aplicación mediante las funciones del módulo
 * react-router-dom
 */
export const MainRoute = ( {handlerLogout} ) => {

    const backgroundMainStyle = 'bg-sky-blue md:grid md:grid-cols-1 md:w-[700px] md:h-full md:p-6 md:place-items-center sm:grid sm:h-auto sm:w-screen sm:place-items-center ';
    const buttonStyle = 'rounded-lg border-b border-l border-gray-blue bg-gray-blue  ring-[1px] ring-blue m-2 shadow-md shadow-black outline-2 focus:outline outline-black';


    return(
        <>
            <div class="md:grid md:grid-cols-3 md:h-screen md:place-content-center sm:flex bg-[#91AEC1]	">
            
                <SideBar handlerLogout={handlerLogout} />
                <Routes>
                    <Route path="/welcome" element={<Welcome backgroundMainStyle={backgroundMainStyle}/>}  />
                    <Route path="consult-info" element={<ConsultInfo backgroundMainStyle={backgroundMainStyle} buttonStyle={buttonStyle}/>} />
                    <Route path="/consult-info/client" element={<ConsultClient backgroundMainStyle={backgroundMainStyle} buttonStyle={buttonStyle}/>} />
                    <Route path="/consult-info/sales" element={<ConsultSales backgroundMainStyle={backgroundMainStyle} buttonStyle={buttonStyle}/>} />
                    <Route path="update" element={<UpdateSelect backgroundMainStyle={backgroundMainStyle} buttonStyle={buttonStyle}/>} />
                    <Route path="/update/update-customer" element={<UpdateCustomer backgroundMainStyle={backgroundMainStyle} buttonStyle={buttonStyle}/>} />
                    <Route path="/update/update-product" element={<UpdateProduct backgroundMainStyle={backgroundMainStyle} buttonStyle={buttonStyle}/>} />
                    <Route path="/update/import-data" element={<ImportData backgroundMainStyle={backgroundMainStyle} buttonStyle={buttonStyle}/>} />

                    <Route path="/" element={<Navigate to="/welcome"/>} />
                </Routes> 

            </div>
        </>
    )
}