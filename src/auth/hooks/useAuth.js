import { useEffect, useReducer, useState } from "react";
import { loginReducer } from "../reducers/loginReducer";
import { loginUser } from "../services/authService";
import { useNavigate } from "react-router-dom";
import { listUsers } from "../services/userService";

/*El objeto initialLogin determina el estado de la sesión,
(por defecto no autorizado) si existe una sesión iniciada 
se tomará el valor (value) de la clave (key) 'login', 
despues de convertirlo a objeto javascript con el método 
JSON.parse()*/
const initialLogin = JSON.parse(sessionStorage.getItem('login')) || {
    isAuth: false,
    isAdmin: false,
    user: undefined,
}

export const useAuth = () => {

    /*Hook reducer para gestionar el inicio y cierre 
    de sesión */
    const [login, dispatch] = useReducer(loginReducer, initialLogin);

    const [loginAdmin, dispatchAdmin] = useReducer(loginReducer, initialLogin);

    const [getUsername ,setGetUsername] = useState();

    const navigate = useNavigate();

   

    /*
    Función para implementar el inicio de sesión 
    heredada a LoginPage a través de "props"
    */
    const handlerLogin = async ({ username, password }) => {

        /*El objeto isLogin verifica que las credenciales
        ingresadas coincidan con el usuario hardcodeado en 
        la función loginUser*/
        // const isLogin = loginUser({ username, password });


        /*Al ingresar las credenciales correctas se cambia 
        el parametro isAuth del objeto login a "true" a través del
        dispatch type 'login' y se toma como payload el nombre
        del usuario para mostrarlo en la barra de navegación */
        //        if (isLogin) {
        try {
            const response = await loginUser({ username, password });
            const token = response.data.token;
            //const user = { username: 'admin' }
            const claims = JSON.parse(window.atob(token.split(".")[1]));
           // console.log(claims);
            const user = { username: response.data.username };
            dispatch({
                type: 'login',
                payload: {user, isAdmin: claims.isAdmin},
            });

            dispatchAdmin({
                type: 'login',
                payload: {user, isAdmin: claims.isAdmin},
            });

            /*sessionStorage.setItem método utilizado para almacenar 
            temporalmente los datos en la sesión del navegador, 
            en este caso se almacena el estado de autenticación y el 
            nombre de usuario en la clave 'login', los datos se borrarán al cerrar 
            el navegador.
            
            El método JSON.stringify se usa para convertir un objeto
            javascript en formato JSON.
            */
            sessionStorage.setItem('login', JSON.stringify({
                isAuth: true,
                isAdmin: claims.isAdmin,
                user,
            }));

            sessionStorage.setItem('token', `Bearer ${token}`);

            navigate('/welcome');

        } catch (error) {

            if (error.response?.status == 401) {
                alert('Error Login', 'Username o password invalidos', 'error');
            } else if (error.response?.status == 403) {
                alert('Error Login', 'No tiene acceso al recurso o permisos', 'error');
            } else {
                throw error;
            }
        }

    }

    const handlerLoginAdmin = async ({ username, password }) => {

        const userList = await listUsers();

        //console.log(userList);

        const user = userList.find((user) => user.username === username);

        //console.log(user);
        //console.log(user.admin);

        if (!user.admin) {
            alert(`El usuario ${user.username} no es administrador`);
            return;
        }

        /*El objeto isLogin verifica que las credenciales
        ingresadas coincidan con el usuario hardcodeado en 
        la función loginUser*/
        // const isLogin = loginUser({ username, password });


        /*Al ingresar las credenciales correctas se cambia 
        el parametro isAuth del objeto login a "true" a través del
        dispatch type 'login' y se toma como payload el nombre
        del usuario para mostrarlo en la barra de navegación */
        //        if (isLogin) {
        try {
            const response = await loginUser({ username, password });
            const usernameFromToken = response.data.username;
           
            const token = response.data.token;
            //const user = { username: 'admin' }
            const claims = JSON.parse(window.atob(token.split(".")[1]));
           // console.log(claims);
            const user = { username: response.data.username };
            //console.log(`user ${response.data.username}`);
            // dispatch({
            //     type: 'login',
            //     payload: {user, isAdmin: claims.isAdmin},
            // });

            dispatchAdmin({
                type: 'login',
                payload: {user, isAdmin: claims.isAdmin},
            });

            /*sessionStorage.setItem método utilizado para almacenar 
            temporalmente los datos en la sesión del navegador, 
            en este caso se almacena el estado de autenticación y el 
            nombre de usuario en la clave 'login', los datos se borrarán al cerrar 
            el navegador.
            
            El método JSON.stringify se usa para convertir un objeto
            javascript en formato JSON.
            */
            sessionStorage.setItem('login', JSON.stringify({
                isAuth: true,
                isAdmin: claims.isAdmin,
                user,
            }));

            sessionStorage.setItem('token', `Bearer ${token}`);

            //navigate('/loginAdmin');

            setGetUsername(loginAdmin.user);
            //console.log("Username from token:", getUsername);



        } catch (error) {

            if (error.response?.status == 401) {
                alert('Error Login', 'Username o password invalidos', 'error');
            } else if (error.response?.status == 403) {
                alert('Error Login', 'No tiene acceso al recurso o permisos', 'error');
            } else {
                throw error;
            }
        }

    }

    /*
    Método para gestionar el cierre de sesión, al despachar
    la acción 'logout' el parametro isAuth del objeto login 
    cambia a 'false'
    */
    const handlerLogout = () => {
        dispatch({
            type: 'logout',
        });

        /*posteriormente se elimina el objeto 
        almacenado en la sesión del navegador */
        sessionStorage.removeItem('login');

        //sessionStorage.rem('login');
        sessionStorage.clear();

        navigate('/login');

        window.location.reload();

    };

  
    return {
        login,
        handlerLogin,
        handlerLogout,
        loginAdmin,
        handlerLoginAdmin,
    }


}