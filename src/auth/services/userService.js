
const BASE_URL = `${import.meta.env.VITE_API_BACKEND_URL}/users`;

const config = () => {
    return {
        headers: {
            "Authorization": sessionStorage.getItem('token'),
            "Content-Type": "application/json"
        }
    }
}

export const createUser = async (payload) => {
    try {
        console.log(sessionStorage.getItem('token'));
        const response = await fetch(BASE_URL, {
            method: 'POST',
            headers: {
                "Authorization": sessionStorage.getItem('token'),
                "Content-Type": "application/json"
            },
            //config,
            body: JSON.stringify(payload)
        });

        const responseData = await response.json();
        console.log(responseData);
        if (response.status === 201) {
            // Procesar la respuesta exitosa
            console.log(responseData.message); // Acceder al mensaje
            return responseData;

        } else if (response.status === 400) {


            const errors = `${JSON.stringify(responseData.username)} ${JSON.stringify(responseData.password)} ${JSON.stringify(responseData.email)}`;

            return responseData;

        } else {
            // Procesar la respuesta con error
            console.log(responseData); // Acceder al mensaje
            return responseData;
        }

    } catch (error) {
        // Procesar el error
        console.error('Error creating customer:', error);
        throw error;
    }
};

export const createAdmin = async (payload) => {
    try {
        console.log(sessionStorage.getItem('token'));
        const response = await fetch(`${BASE_URL}/admin`, {
            method: 'POST',
            headers: {
                "Authorization": sessionStorage.getItem('token'),
                "Content-Type": "application/json"
            },
            //config,
            body: JSON.stringify(payload)
        });

        const responseData = await response.json();
        console.log(responseData);
        if (response.status === 201) {
            // Procesar la respuesta exitosa
            console.log(responseData.message); // Acceder al mensaje
            return responseData;

        } else if (response.status === 400) {


            const errors = `${JSON.stringify(responseData.username)} ${JSON.stringify(responseData.password)} ${JSON.stringify(responseData.email)}`;

            return responseData;

        } else {
            // Procesar la respuesta con error
            console.log(responseData); // Acceder al mensaje
            return responseData;
        }

    } catch (error) {
        // Procesar el error
        console.error('Error creating customer:', error);
        throw error;
    }
};

export const checkoutEmail = async (email) => {

    const response = await fetch(`${BASE_URL}/users/verifyEmail?email=${email}`);

    const responseData = await response.json();
    if (response.status === 201) {
        // Procesar la respuesta exitosa
        console.log(responseData.message); // Acceder al mensaje
    } else if (response.status === 409) {
        // Procesar la respuesta con conflicto
        console.log(responseData.message); // Acceder al mensaje
    } else {
        // Procesar la respuesta con error
        console.error(responseData.message); // Acceder al mensaje
    }

    return responseData;
};

export const updatePassword = async (id, payload) => {
    try {

        const response = await fetch(`${BASE_URL}/users/updatePassword/${id}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload)
        });

        const responseData = await response.json();
        if (response.status === 201) {
            // Procesar la respuesta exitosa
            console.log(responseData.message); // Acceder al mensaje
        } else if (response.status === 409) {
            // Procesar la respuesta con conflicto
            console.log(responseData.message); // Acceder al mensaje
        } else {
            // Procesar la respuesta con error
            console.error(responseData.message); // Acceder al mensaje
        }

        return responseData;

    } catch (error) {
        // Procesar la respuesta con error
        console.error(responseData.message); // Acceder al mensaje
    }
};

export const listUsers = async () => {
    const response = await fetch(BASE_URL);
    const responseBody = await response.json();
    const usersFound = responseBody;
    return usersFound;
};

export const updateUser = async (id, payload) => {

        const {username, password, email} = payload;

        console.log(username, password, email);
        console.log(id);
    try {
        console.log(sessionStorage.getItem('token'));
        const response = await fetch(`${BASE_URL}/${id}`, {
            method: 'PUT',
            headers: {
                "Authorization": sessionStorage.getItem('token'),
                "Content-Type": "application/json"
            },
            body: JSON.stringify({username, password, email})
        });

        const responseData = await response.json();
        console.log(responseData);
        if (response.status === 201) {
            // Procesar la respuesta exitosa
            console.log(responseData.message); // Acceder al mensaje
            return responseData;

        } else if (response.status === 400) {

            return responseData;

        } else {
            // Procesar la respuesta con error
            console.log(responseData); // Acceder al mensaje
            return responseData;
        }
    } catch (error) {
        console.error('Error creating customer:', error);
        throw error;
    }
};

export const deleteUser = async (id) => {
    try {
        const response = await fetch(`${BASE_URL}/${id}`, {
            method: 'DELETE',
            headers: {
                "Authorization": sessionStorage.getItem('token'),
                "Content-Type": "application/json"
            },
        });

        //const responseData = await response.json();

        if (response.status === 204) {
            return true;
        } else {
            return false;
        }

    } catch (error) {
        console.error('Error deleting customer:', error);

    }
};