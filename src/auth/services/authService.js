import axios from "axios";

export const loginUser =async ({username, password}) => {
    try {
        return await axios.post(`${import.meta.env.VITE_API_BACKEND_URL}/login`, {
            username,
            password
        });

    } catch (error) {
        throw error;
    }
    //return (userLogin.username === 'admin' && userLogin.password === '12345');
}