import { useEffect, useState } from "react";
import ModalWindowNewUser from "./ModalWindowNewUser";
import { deleteUser, listUsers } from "../services/userService";
import { useAuth } from "../hooks/useAuth";
import { FaCheck, FaPencilAlt } from "react-icons/fa";
import { IoClose } from "react-icons/io5";
import { TiDelete, TiDeleteOutline } from "react-icons/ti";
import ModalWindowUpdateUser from "./ModalWindowUpdateUser";

export const UsersDashboard = () => {

    const initialUserForm = {
        username: '',
        password: '',
        email: ''
    };

    const [modalNewUser, setModalNewUser] = useState(false);

    const [modalUpdateUser, setModalUpdateUser] = useState(false);

    const [selectedUser, setSelectedUser] = useState(initialUserForm);

    const [userFound, setUserFound] = useState([]);

    const [usernameFromToken, setUsernameFromToken] = useState('');

    const { loginAdmin, handlerLogout } = useAuth();

    const handleListCustomer = async () => {
        //setUsernameFromToken(loginAdmin.user.username)
        const userResult = await listUsers();
        setUserFound(userResult);
        //console.log(userFound);
        //console.log(loginAdmin.user.username);
    }

    //console.log(`getUsername ${getUsername}`);

    const backgroundMainStyle = 'bg-sky-blue md:grid md:grid-cols-1 md:w-[700px] md:h-[500px] md:p-6 md:place-items-center sm:grid sm:h-auto sm:w-screen sm:place-items-center place-self-center	';

    const buttonStyle = 'rounded-lg border-b border-l border-gray-blue bg-gray-blue  ring-[1px] ring-blue m-2 shadow-md shadow-black outline-2 focus:outline outline-black p-2';


    useEffect(() => {
        const loginDetails = JSON.parse(sessionStorage.getItem('login')) || {
            isAuth: false,
            isAdmin: false,
            user: { username: '' }
        };
        setUsernameFromToken(loginDetails.user.username);
        //console.log(loginDetails.user.username);

        handleListCustomer();
    }, []);

    //console.log(userFound);

    const handlerEditUser = async (user) => {

        //console.log(user);

        //const { id, username, email } = user;

        setSelectedUser(user);

        setModalUpdateUser(true);

        console.log(selectedUser);
    };

    const handleDeleteUser = async (id) => {

        if (id === selectedUser.id) { alert('No puedes eliminar tu propio usuario'); return; }

        if (confirm("¿Estás seguro de que deseas eliminar el usuario id:" + id)) {

            const response = await deleteUser(id);
            console.log(response);
            if (response) {

                alert('Usuario eliminado');
            } else {
                alert('Ocurrió un problema con la eliminación del usuario id:' + id);
            }
            
            window.location.reload();

        } 
    }

    const handlerLogoutAdminSession = () => {
        handlerLogout();
        window.location.reload();
    }

    //console.log(selectedUser);

    return (
        <div className="h-screen grid grid-cols-1  content-center bg-[#91AEC1]">
            {modalNewUser && (
                <ModalWindowNewUser onClose={setModalNewUser} initialUserForm={initialUserForm} />
            )}

            {modalUpdateUser && (
                <ModalWindowUpdateUser
                    onClose={setModalUpdateUser}
                    initialUserForm={initialUserForm}
                    selectedUser={selectedUser} />
            )}

            <div className="flex justify-self-center md:w-[700px] sm:w-full h-10 bg-gray-blue">
                <div className="grid justify-items-end  bg-blue md:w-[700px] sm:w-full h-min p-0">
                    <div className="">

                        {/* <button className="bg-black p-2 text-zinc">
                            Admin
                        </button> */}

                        <div class="dropdown">
                            <a class="btn btn-secondary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                {usernameFromToken}
                            </a>

                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">
                                    {userFound && (
                                        userFound.filter(user => user.username == usernameFromToken).map(user =>

                                            <button
                                                onClick={() => handlerEditUser(user)}
                                            >
                                                Editar Usuario
                                            </button>
                                        ))}

                                </a></li>
                                <li><a class="dropdown-item" href="#">
                                    <button onClick={() => handlerLogoutAdminSession()}>
                                        Cerrar Sesión
                                    </button>
                                    </a></li>
                            </ul>
                        </div>

                    </div>
                </div>






            </div>

            <div className={backgroundMainStyle}>

                <div class="grid grid-flow-row auto-rows-max h-auto place-content-center py-0" >
                    <div class="grid grid-cols-1 w-[600px] self-start">
                        <p class="text-xs font-light italic ">Usuarios Registrados</p>
                        <div id="table-wrapper" class="w-full">
                            <div id="table-scroll-2" class="scrollbar-thin scrollbar-webkit">
                                <table class="table w-full table-auto ...">
                                    <thead class="table-header-group">
                                        {/* <div className="flex justify-around"> */}

                                        <tr class="table-row">
                                            <th className="text-center">
                                                Nombre de Usuario
                                            </th>
                                            <th
                                                class="table-cell text-center px-2 border-dotted 
                                            border-neutral-500 border-r-0  align-middle"
                                            >
                                                Email
                                            </th>
                                            <th
                                                class="table-cell text-center px-1 border-dotted 
                                            border-neutral-500 border-r-2  align-middle"
                                            >
                                                Administrador
                                            </th>
                                            <th
                                                class="table-cell text-center px-1 border-dotted 
                                            border-neutral-500 border-r-2  align-middle"
                                            >
                                                Acciones
                                            </th>
                                        </tr>
                                        {/* </div> */}

                                    </thead>
                                    <tbody>
                                        {userFound && (
                                            userFound
                                                .filter(user => user.username !== usernameFromToken)
                                                .map(user => (
                                                    //  <div className="flex justify-between">
                                                    <tr key={user.id} className="w-[500px]">
                                                        <td>
                                                            <div className="grid justify-items-center">
                                                                {user.username}
                                                            </div>
                                                        </td>

                                                        <td>
                                                            <div className="grid justify-items-center">
                                                                {user.email}
                                                            </div>
                                                        </td>

                                                        <td>
                                                            <div className="grid justify-items-center">
                                                                {user.admin ? <FaCheck /> : <IoClose />}
                                                            </div>
                                                        </td>

                                                        <td>
                                                            <div className="flex place-content-center">
                                                                <button className="px-2"
                                                                    onClick={() => handlerEditUser(user)}
                                                                >
                                                                    <FaPencilAlt />
                                                                </button>
                                                                <button onClick={() => handleDeleteUser(user.id)}>
                                                                    <TiDelete />
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    //  </div>
                                                ))
                                        )
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div className="pt-10 place-self-center">

                        <button className={buttonStyle}
                            onClick={() => setModalNewUser(true)}
                        > Crear Usuario
                        </button>
                    </div>

                </div>

            </div>

        </div>
    )
};