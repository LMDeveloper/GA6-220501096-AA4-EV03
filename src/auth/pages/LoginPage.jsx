import { FaUser } from "react-icons/fa6";
import { FaUserCircle } from "react-icons/fa";
import { GiPadlock } from "react-icons/gi";
import { useState } from "react";
import ModalWindowNewUser from "./ModalWindowNewUser";
import ModalWindowResetUser from "./ModalWindowResetUser";
import { NavLink } from "react-router-dom";

const initialLoginForm = {
    username: '',
    password: '',
}

export const LoginPage = ({ handlerLogin }) => {

    const inputNameStyle = "bg-zinc-300 px-2 w-[160px] italic font-extralight pl-2 pt-1 h-8 shadow-lg shadow-neutral-600 border-r-4 border-neutral-900 border-double";
    const inputBoxStyle = "rounded-tr-2xl  bg-neutral-400 outline-2 focus:outline outline-neutral-400 ring-0 ring-neutral-400	text-sm h-8 w-[200px] text-inherit placeholder-zinc-700 shadow-md shadow-neutral-600 focus:bg-neutral-300";
    const buttonStyle = 'rounded-lg border-b border-l border-gray-blue bg-gray-blue  ring-[1px] ring-blue m-2 shadow-md shadow-black outline-2 focus:outline outline-black';


    const [showModalNewUser, setShowModalNewUser] = useState(false);

    const [showModalResetUser, setShowModalResetUser] = useState(false);

    /*Hook para inicializar el formulario Login */
    const [loginForm, setLoginForm] = useState(initialLoginForm);

    /*Se desestructura el objeto loginForm en los parametros
    usuario y contraseña, los cuales se usarán para validar
    el acceso*/
    const { username, password } = loginForm;

    /*Método para capturar y asignar el valor ingresado
    de cada Input al objeto loginForm  */
    const onInputChange = ({ target }) => {
        const { name, value } = target;
        setLoginForm({
            ...loginForm,
            [name]: value,
        })
    }

    /*Método para validar el formulario*/
    const onSubmit = (event) => {
        event.preventDefault();
        if (!username || !password) {
            alert('Error de validacion', 'Username y password requeridos', 'error');
        }

        //Implementación del login
        handlerLogin({ username, password });

        //Reiniciar el formulario
        setLoginForm(initialLoginForm);
    }

    return (

        <div class="flex flex-nowrap m-0 place-content-center bg-[#91AEC1]" >
            {showModalNewUser && (
                <ModalWindowNewUser 
                    onClose={() => setShowModalNewUser(false)} 
                    inputNameStyle={inputNameStyle}
                    inputBoxStyle={inputBoxStyle}
                    buttonStyle={buttonStyle}
                    />
            )}

            {showModalResetUser && (
                <ModalWindowResetUser 
                    onClose={() => setShowModalResetUser(false)} 
                    inputNameStyle={inputNameStyle}
                    inputBoxStyle={inputBoxStyle}
                    buttonStyle={buttonStyle}
                    />
            )}


            <div class="grid lg:grid-cols-2 gap-0 lg:m-24 sm:m-0 ">
                <div
                    class="bg-blue py-[100px] place-content-center lg:h-full sm:h-2">
                    <h2 class="text-black text-center text-2xl px-10">Sistema de Gestión de Relaciones con los Clientes</h2>
                </div>
                <div class="bg-sky-blue p-[50px] h-full">

                    <div class="grid grid-cols-1 place-items-center">

                        <FaUser class="w-24 h-24 my-[15px]" />
                    </div>

                    {/* <h2 class="py-5">Login Page</h2> */}
                    <form onSubmit={onSubmit}>
                        <div class="grid grid-cols-1 justify-items-center">
                            <div class="flex items-stretch h-[50px] ">
                                <input
                                    class="my-2 bg-zinc rounded-r-full shadow-md shadow-black"
                                    placeholder="Username"
                                    name="username"
                                    value={username}
                                    onChange={onInputChange}
                                />
                                <FaUserCircle class="self-start m-[10px] w-[30px] h-[30px]" />
                            </div>
                            <div class="flex items-stretch h-[50px]">
                                <input
                                    class=" my-2 rounded-r-full bg-zinc shadow-md shadow-black"
                                    placeholder="Password"
                                    type="password"
                                    name="password"
                                    value={password}
                                    onChange={onInputChange}
                                />
                                <GiPadlock class="self-start m-[10px] w-[30px] h-[30px]" />
                            </div>
                        </div>
                        <div class="grid grid-cols-1 place-items-center py-6">
                            <button
                                class="h-full w-24 rounded-lg border-b border-l border-gray-blue bg-gray-blue  ring-[1px] ring-blue m-2 shadow-md shadow-black outline-2 focus:outline outline-black"
                                type="submit">
                                INGRESAR
                            </button>
                        </div>
                    </form>
                    <div class="grid grid-cols-2 text-blue-950 text-sm">
                        {/* <button
                            class="place-self-start"
                            onClick={() => setShowModalNewUser(true)}
                        >Nuevo Usuario</button> */}
                        <button 
                            class="place-self-center"
                            onClick={() => setShowModalResetUser(true)}
                        >
                            <p className="text-xs">
                                Olvidaste tu contraseña?
                            </p>
                        </button>
                         <NavLink className="place-self-center nav-link-button" to="/handleUsers">
                            <button 
                                class=""
                                //onClick={() => setShowModalResetUser(true)}
                            >
                                <p className="text-xs text-center	">
                                    Gestionar Usuarios
                                </p>
                            </button>
                         </NavLink>
                    </div>
                </div>

            </div>

        </div>
    );
}