import { useState } from "react";
import { checkoutEmail, updatePassword } from "../services/userService";

const ModalWindowResetUser = ({ onClose, inputNameStyle, inputBoxStyle, buttonStyle }) => {

    const [showPassword, setShowPassword] = useState(false);

    const [idUserToModify, setIdUserToModify] = useState(null);

    const initialPasswordForm = {
        newPassword: '',
        repeatPassword: ''
    };

    const [changePassword, setChangePassword] = useState(initialPasswordForm);
    const {newPassword, repeatPassword} = changePassword;

    const onInputChangeEmail = ({ target }) => {

        /*
         * Dentro de la función, se desestructura el evento
         * para obtener la propiedad target, que hace referencia
         * al elemento que desencadenó el evento, es decir, el
         * campo de entrada (input) en el que el usuario está
         * escribiendo.
         *
         * const { target } = event;
         */
        const { name, value } = target;

        setEmail(value);
    };

    const [email, setEmail] = useState('');

    //console.log(email);

    const onInputChangePassword = ({target}) => {
        const {name, value} = target;

        setChangePassword(
            {

                ...changePassword,
                [name]: value,
            }
        );
    };

    const handleVerifyEmail = async (email) => {
        if (email === '') return;

        const emailInput = await checkoutEmail(email);

        if (emailInput.object !== null) {
            setShowPassword(true);
            setIdUserToModify(emailInput.object.id);

        }

        if (emailInput.object === null) alert(emailInput.message); return;
    };

    const handleResetPassword = async (id, payload) => {

        const {newPassword, repeatPassword} = payload;

        console.log(newPassword);
        console.log(repeatPassword);
        
        if (!(newPassword === repeatPassword)){
            alert('Las contraseñas no coinciden');
            return;

        } else {
            const passwordInput = await updatePassword(id, {password: newPassword});
            alert(passwordInput.message);
            window.location.reload();
        }


    }

    // console.log(idUserToModify);
    // console.log(changePassword);

    return (
        <div>
            <div className='fixed inset-0 right-30 bg-black
        bg-opacity-25 backdrop-blur-sm flex
        justify-center items-center z-30 '
            >
                <div className='w-[300x] px-3 sm:w-[330px] sm:px-0
            grid grid-rows-1 z-40'
                >

                    <button className='text-white text-xl
                    place-self-end'
                        onClick={() => onClose()}
                    >
                        x
                    </button>
                    <div className='bg-sky-blue p-2 rounded h-[auto]
                grid grid-rows-1 w-[400px]  space-y-4
                '>
                        <h1 className="text-2xl text-center font-medium	 py-2 shadow-lg	">Olvidaste tu contraseña? </h1>

                        {showPassword ?
                            <>
                                <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-lg">
                                    <div
                                        class={inputNameStyle}>
                                        Nuevo Password
                                    </div>
                                    <input
                                        type="password"
                                        placeholder=""
                                        class={inputBoxStyle}
                                        name='newPassword'
                                        value={newPassword}
                                        onChange={onInputChangePassword}
                                    />
                                </div>
                                <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-lg">
                                    <div
                                        class={inputNameStyle}>
                                        Repetir Password
                                    </div>
                                    <input
                                        type="password"
                                        placeholder=""
                                        class={inputBoxStyle}
                                        name='repeatPassword'
                                        value={repeatPassword}
                                        onChange={onInputChangePassword}
                                    />
                                </div>

                                <div className="place-content-center">
                                    <div class="flex flex-wrap	 h-min place-content-center pr-0">

                                        <div class=" ">
                                            <button class={buttonStyle + " p-1"}
                                                onClick={() => handleResetPassword(idUserToModify, changePassword)}
                                            >
                                                {/* <img class="h-8 w-8 ml-[2px]" src="/guardar.png" /> */}
                                                REINICIAR PASSWORD
                                            </button>
                                        </div>

                                    </div>
                                </div>


                            </>
                            :
                            <>

                                <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-lg">
                                    <div
                                        class={inputNameStyle}>
                                        Email
                                    </div>
                                    <input
                                        type="text"
                                        placeholder=""
                                        class={inputBoxStyle}
                                        name='email'
                                        value={email}
                                        onChange={onInputChangeEmail}
                                    />
                                </div>
                                <div className="place-content-center">
                                    <div class="flex flex-wrap	 h-min place-content-center pr-0">

                                        <div class=" ">
                                            <button class={buttonStyle + " p-1"}
                                                onClick={() => handleVerifyEmail(email)}
                                            >
                                                {/* <img class="h-8 w-8 ml-[2px]" src="/guardar.png" /> */}
                                                COMPROBAR EMAIL
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </>

                        }





                    </div>
                </div>
            </div>
        </div>

    )

}
export default ModalWindowResetUser;
