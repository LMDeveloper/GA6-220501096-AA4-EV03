import React, { useState } from "react";
import { createAdmin, createUser } from "../services/userService";
import Input from "postcss/lib/input";
import { Switch } from "@mui/material";


const inputNameStyle = "bg-zinc-300 px-2 w-[140px] italic font-extralight pl-2 pt-1 h-8 shadow-lg shadow-neutral-600 border-r-4 border-neutral-900 border-double";
const inputBoxStyle = "rounded-tr-2xl  bg-neutral-400 outline-2 focus:outline outline-neutral-400 ring-0 ring-neutral-400	text-sm h-8 w-[200px] text-inherit placeholder-zinc-700 shadow-md shadow-neutral-600 focus:bg-neutral-300";
const buttonStyle = 'rounded-lg border-b border-l border-gray-blue bg-gray-blue  ring-[1px] ring-blue m-2 shadow-md shadow-black outline-2 focus:outline outline-black';

const options = [
    { value: 'option1', label: 'Opción 1' },
    { value: 'option2', label: 'Opción 2' },
    { value: 'option3', label: 'Opción 3' },
];


const ModalWindowNewUser = ({ onClose }) => {

    const [checked, setChecked] = React.useState(false);

    const handleChange = (event) => {
        setChecked(event.target.checked);
    }

    const [selectedOption, setSelectedOption] = useState(null);

    const handleToggle = (value) => {
        setSelectedOption(value);
    };

    const initialUserForm = {
        username: '',
        password: '',
        email: ''
    };

    const initialErrorsForm = {
        username: '',
        password: '',
        email: ''
    };

    const [userForm, setUserForm] = useState(initialUserForm);
    const { username, password, email } = userForm;


    const [errorForm, setErrorForm] = useState(initialErrorsForm);

    const onInputChange = ({ target }) => {

        /*
         * Dentro de la función, se desestructura el evento
         * para obtener la propiedad target, que hace referencia
         * al elemento que desencadenó el evento, es decir, el
         * campo de entrada (input) en el que el usuario está
         * escribiendo.
         *
         * const { target } = event;
         */
        const { name, value } = target;

        setUserForm(
            {
                /**
                 * [name] contiene el nombre del campo de entrada que
                 * cambió (puede ser 'name', 'address' o 'phoneNumber').
                 * "value" contiene el valor que el usuario ha ingresado
                 * en el campo de entrada.
                 */
                ...userForm,
                [name]: value,
            }
        );
    };

    // const onClose = () => {
    //     setModalEditUser(false);
    // }

    const handleSaveUser = async (userForm) => {
        //if (username === '' || password === '' || email === '') return;

        try {

            let userInput = null;

            if (checked) {
                userInput = await createAdmin(userForm);
            }

            userInput = await createUser(userForm);

            if (!userInput.message) {
                console.log(userInput.password);
                setErrorForm(
                    {
                        username: userInput.username,
                        password: userInput.password,
                        email: userInput.email
                    }
                );
                //alert(userInput.password);
                //window.location.reload();

            } else {
                alert(userInput.message);
                window.location.reload();
            }


        } catch (error) {
            if (error.message.includes('Duplicate entry') && error.message.includes('for key')) {
                alert('Usuario o email ya se encuentra registrado');

            } else {
                alert('Error al crear el usuario');

            }

            console.error('Error creating user:', error);

            window.location.reload();
        }
    };

    //console.log(errorForm.email);

    return (
        <div>
            <div className='fixed inset-0 right-30 bg-black
        bg-opacity-25 backdrop-blur-sm flex
        justify-center items-center z-30 '
        
            >
                <div className='w-[300x] px-3 sm:w-[400px] sm:px-0
            grid grid-rows-1 z-40'
            
                >

                    <button className='text-white text-xl
                    place-self-end'
                    onClickCapture={() => onClose()}
                    >
                        x
                    </button>
                    <div className='bg-sky-blue p-2 rounded h-[auto]
                grid grid-rows-1 w-full  space-y-4 
                '>
                        <h1 className="text-2xl text-center font-medium	 py-2 shadow-lg	">Registrar Usuario</h1>

                        <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-lg">
                            <div
                                class={inputNameStyle}>
                                Usuario
                            </div>
                            <input
                                type="text"
                                placeholder=""
                                class={inputBoxStyle}
                                name='username'
                                value={username}
                                onChange={onInputChange}
                            />
                        </div>
                        <p className="text-xs text-red" > {errorForm?.username}  </p>
                        <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-lg">
                            <div
                                class={inputNameStyle}>
                                Contraseña
                            </div>
                            <input
                                type="password"
                                placeholder=""
                                class={inputBoxStyle}
                                name='password'
                                value={password}
                                onChange={onInputChange}
                            />
                        </div>
                        <p className="text-xs text-red" > {errorForm?.password}  </p>
                        <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-lg">
                            <div
                                class={inputNameStyle}>
                                Email
                            </div>
                            <input
                                type="text"
                                placeholder=""
                                class={inputBoxStyle}
                                name='email'
                                value={email}
                                onChange={onInputChange}
                            />
                        </div>
                        <p className="text-xs text-red" > {errorForm?.email}  </p>
                        <div className="flex flex-row h-8 border-b-1 border-neutral-400 shadow-lg">
                            <div
                                class={inputNameStyle}>
                                Administrador
                            </div>

                            <Switch
                                checked={checked}
                                onChange={handleChange}
                                inputProps={{ 'aria-label': 'controlled' }}
                            />


                        </div>

                        <div className="place-content-center ">
                            <div class="flex flex-wrap	 h-min place-content-center pr-0">

                                <div class=" ">
                                    <button class={buttonStyle + " p-1"}
                                        onClick={() => handleSaveUser(userForm)}
                                    >
                                        <img class="h-8 w-8 ml-[2px]" src="/guardar.png" />
                                    </button>
                                </div>

                                {/* <div class="">
                                    <button class={buttonStyle + " p-1"}
                                        //onClick={}
                                    >
                                        <img class="h-8 w-8 ml-[2px]" src="/editar.png" />
                                    </button>
                                </div> */}

                                <div class="">
                                    <button class={buttonStyle + " p-1"}
                                        onClick={() => setUserForm(initialUserForm)}
                                    >
                                        <img class="h-8 w-8 ml-[2px]" src="/eraser-icon.png" />
                                    </button>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    );

};
export default ModalWindowNewUser;