import { useState } from "react";
import { useAuth } from "../hooks/useAuth";
import { FaUser } from "react-icons/fa6";
import { FaUserCircle } from "react-icons/fa";
import { GiPadlock } from "react-icons/gi";

export const AuthFormPageAdmin = ({handlerLoginAdmin, handlerLogout}) => {

    const initialLoginForm = {
        username: '',
        password: '',
    };

    /*Hook para inicializar el formulario Login */
    const [loginForm, setLoginForm] = useState(initialLoginForm);

    /*Se desestructura el objeto loginForm en los parametros
    usuario y contraseña, los cuales se usarán para validar
    el acceso*/
    const { username, password } = loginForm;

    /*Método para capturar y asignar el valor ingresado
    de cada Input al objeto loginForm  */
    const onInputChange = ({ target }) => {
        const { name, value } = target;
        setLoginForm({
            ...loginForm,
            [name]: value,
        })
    }

    /*Método para validar el formulario*/
    const onSubmit = (event) => {
        event.preventDefault();
        if (!username || !password) {
            alert('Error de validacion', 'Username y password requeridos', 'error');
        }

        //Implementación del login
        handlerLoginAdmin({ username, password });

        //Reiniciar el formulario
        setLoginForm(initialLoginForm);
    }
 
    

    return (
        <>
            <div class="grid grid-flow-row auto-rows-max h-screen place-content-center bg-[#91AEC1]" >

                <div class="bg-sky-blue p-[40px] h-full">

                    <div class="grid grid-cols-1 place-items-center">

                        <h2 className="font-normal	text-base pb-0">Ingrese sus credenciales de Administrador</h2>
                        <FaUser class="w-12 h-12 my-[15px]" />
                    </div>

                    <form onSubmit={onSubmit} >
                        <div class="grid grid-cols-1 justify-items-center">
                            <div class="flex items-stretch h-[50px] ">
                                <input
                                    class="my-2 bg-zinc rounded-r-full shadow-md shadow-black"
                                    placeholder="Username"
                                    name="username"
                                    value={username}
                                    onChange={onInputChange}
                                />
                                <FaUserCircle class="self-start m-[10px] w-[30px] h-[30px]" />
                            </div>
                            <div class="flex items-stretch h-[50px]">
                                <input
                                    class=" my-2 rounded-r-full bg-zinc shadow-md shadow-black"
                                    placeholder="Password"
                                    type="password"
                                    name="password"
                                    value={password}
                                    onChange={onInputChange}
                                />
                                <GiPadlock class="self-start m-[10px] w-[30px] h-[30px]" />
                            </div>
                        </div>
                        <div class="grid grid-cols-1 place-items-center pt-6">
                            <button
                                class="h-full w-24 rounded-lg border-b border-l border-gray-blue bg-gray-blue  ring-[1px] ring-blue m-2 shadow-md shadow-black outline-2 focus:outline outline-black"
                                type="submit"
                            //onClick={() => setIsAuthAdmin(true)}
                            >
                                INGRESAR
                            </button>
                        </div>
                    </form>
                </div>
            </div >
        </>
    )
}