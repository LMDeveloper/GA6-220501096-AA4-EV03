import React, { useState } from "react"
import { FaUser, FaUserCircle } from "react-icons/fa"
import { GiPadlock } from "react-icons/gi"
import ModalWindowNewUser from "./ModalWindowNewUser";
import { Navigate, Route, Routes } from "react-router-dom";
import { useAuth } from "../hooks/useAuth";
import { AuthFormPageAdmin } from "./AuthFormPageAdmin";
import { UsersDashboard } from "./UsersDashboard";

export const LoginAdminPage = () => {

    const { loginAdmin, handlerLoginAdmin, handlerLogout, handlerGetUserName } = useAuth();




    console.log(`loginAdmin.isAuth ${loginAdmin.isAuth}`);

    return (
        <React.Fragment>
            {
                !loginAdmin.isAuth ? (

                    <>

                        {/* <React path="/loginAdmin/auth"
                            element={<AuthFormPageAdmin
                                handlerLoginAdmin={handlerLoginAdmin}
                                handlerLogout={handlerLogout} />}
                        /> */}

                        <AuthFormPageAdmin
                            handlerLoginAdmin={handlerLoginAdmin}
                            handlerLogout={handlerLogout} />

                        {/* <Route path='/loginAdmin/*' element={<Navigate to="/loginAdmin/auth" />} /> */}

                    </>

                ) : (
                    // <Route path="/loginAdmin/dashboard"
                    //     element={<UsersDashboard />} />

                    <UsersDashboard />

                )
            }
        </React.Fragment>
    )
}