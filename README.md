SERVICIO NACIONAL DE APRENDIZAJE 

ANALISIS Y DESARROLLO DE SOFTWARE (2758333)

PRESENTADO POR: LUIS MIGUEL RODRIGUEZ VARGAS

INSTRUCTOR: ING. FERNANDO FORERO GOMEZ

Arquitectura del Sistema CRM

El sistema CRM se sustenta en una arquitectura robusta y escalable, diseñada para gestionar de manera eficiente la información de los clientes. Esta arquitectura se compone de los siguientes elementos:

Backend: Desarrollado en Java y utilizando el framework Spring, el backend actúa como el cerebro del sistema. Se encarga de procesar las solicitudes, acceder a la base de datos y ejecutar las operaciones necesarias para mantener la integridad de la información. Spring proporciona un entorno de desarrollo sólido y facilita la gestión de las diferentes capas de la aplicación.

Frontend: Construido con JavaScript y React, el frontend ofrece una interfaz de usuario intuitiva y dinámica. React permite crear interfaces de usuario complejas de manera eficiente y modular, proporcionando una experiencia de usuario óptima.

Base de datos: Utilizamos MySQL como sistema de gestión de bases de datos relacionales. MySQL almacena de forma estructurada toda la información de los clientes, sus interacciones, historial de compras y otros datos relevantes. Su alta performance y fiabilidad garantizan un acceso rápido y seguro a la información.
____

Ejecutar los comandos:
(Instalar dependencias)
npm install
(Ejecutar servidor local)
npm run dev
(abrir la ubicación)
http://localhost:5173/

Usuario: admin
Contraseña: 12345

___

Actualización: 
   - Paleta de colores
   - Diseño responsive (smartphone)

Inicio de sesión

![Login (Usuario: admin - Password: 12345)](/public/login.png)
![Login Resposive Smartphone](/public/login-responsive.png)

![Home)](/public/welcome.png)
![Home Resposive Smartphone)](/public/welcome-responsive.png)

Gestión de usuarios

![Consultar Info (Gestionar Usuarios)](/public/usuarios.png)

Consultar Informacion

![Consultar Informacion)](/public/consultar-info2.png)

Consultar Cliente

![Consultar Cliente](/public/cliente-info2.png)

Reporte de Ventas

![Reporte de Ventas](/public/reporte-de-ventas.png)

Balance

![Reporte de Ventas](/public/balance.png)

Actualizar / Importar datos

![Actualizar Info)](/public/importar.png)

Actualizar Cliente

![Actualizar Cliente)](/public/actualizar-cliente2.png)

Actualizar Producto

![Actualizar Cliente)](/public/actualizar-producto.png)

Importar Datos

![Actualizar Cliente)](/public/importar-info.png)

Formato Excel

![Actualizar Cliente)](/public/ARCHIVO-EXCEL.png)

Información Importada 

![Actualizar Cliente)](/public/datos-importados.png)
