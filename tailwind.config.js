import { BiBorderRadius } from 'react-icons/bi'

/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    colors:{
      'black': '#0a0a0a',
      'blue': '#508CA4',
      'sky-blue': '#BFD7EA',
      'gray-blue': '#91AEC1',
      'yellow': '#F5CB5C',
      'zinc': '#f4f4f5',
      'transparent-bg': '#f4f4f500',
      'red': '#dc2626'
    },
    extend: {},
  },
  plugins: [
    function({addUtilities}){
      const newUtilities = {
        ".scrollbar-thin" : {
          scrollbarWidth: "thin",
          scrollbarColor: "rgb(31 29 29) white"
        },
        ".scrollbar-webkit": {
          "&::-webkit-scrollbar" : {
            width: "8px"
          },
          "&::-webkit-scrollbar-track": {
            background: "white"
          },
          "&::-webkit-scrollbar-thumb": {
            backgroundColor: "rgb(31 41 55)",
            borderRadius: "20px",
            border: "1px solid white"
          }
        }
      }
      addUtilities(newUtilities, ["responsive", "hover"])
    }
  ],
}

